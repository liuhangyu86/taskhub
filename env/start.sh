#!/bin/bash

COMPOSE_FILE=docker-compose.yml

function initdb() {
  startup=`docker logs mysql  2>&1| grep --line-buffered "Ready for start up"`
  while [ -z "$startup" ]
  do
    echo "mysql is not init, please waiting ..."
    startup=`docker logs mysql  2>&1| grep --line-buffered "port: 33060"`
    sleep 1
  done

  docker exec -it mysql bash -c "mysql  -u root -P 3306 --force < /init.sql"
  if [ $? -ne 0 ]; then
    echo "ERROR !!!! Init failed"
    exit 1
  else
    echo "Init Database Success"
    exit 1
  fi
}


function networkUp() {
  docker-compose -f $COMPOSE_FILE up -d 2>&1
  if [ $? -ne 0 ]; then
    echo "ERROR !!!! Unable to start network"
    exit 1
  fi

  #sleep 20
  #grant all on *.* to root@localhost with grant option;
  #ip=`docker inspect mysql  --format '{{ .NetworkSettings.Networks.env_default.IPAddress }}'`
  #docker exec -it mysql bash -c "echo $ip && /usr/bin/mysql  -h$ip -uroot -p123456 --force < /init.sql"

  #docker exec -it mysql sh -c "sleep 2&& echo `hostname -i` && echo `awk 'END{print $1}' /etc/hosts`&& echo $ip && mysql  -h$ip -uroot -p123456"
  #docker exec  -it mysql sh -c "echo $ip && /usr/bin/mysql  -h$ip -uroot -p123456"
  #sh -c "docker exec -it mysql sh -c "mysql  -h`hostname -i` -uroot -p123456""
  #mysql  grant all on *.* to root@localhost with grant option;
  initdb
}

function networkDown() {
  docker-compose -f $COMPOSE_FILE down --volumes --remove-orphans
}


function cleandata() {
  sudo rm -rf ./mysql_config/mysql_data
  sudo rm -rf ./redis_config/redis_data
}

# Parse commandline args
if [ "$1" = "-m" ]; then # supports old usage, muscle memory is powerful!
  shift
fi
MODE=$1
shift

#Create the network using docker compose
if [ "${MODE}" == "up" ]; then
  networkUp
elif [ "${MODE}" == "down" ]; then ## Clear the network
  networkDown
elif [ "${MODE}" == "clean" ]; then 
  cleandata  
else
  exit 1
fi
