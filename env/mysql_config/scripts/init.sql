-- mysql --force < script.sql
drop user  IF EXISTS  'admin'@'%';
CREATE USER 'admin'@'%' IDENTIFIED BY 'joors';
GRANT ALL PRIVILEGES ON joors.* TO 'admin'@'%';
ALTER USER 'admin'@'%' IDENTIFIED BY '123456';
FLUSH PRIVILEGES;

CREATE DATABASE IF NOT EXISTS joors CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
use mysql;