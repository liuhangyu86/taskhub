package modcom

import (
	"sync"
	"time"

	"github.com/jinzhu/gorm"
)

type TradeCoinStatus struct {
	CoinSymbol *string    `gorm:"column:coin_symbol" sql:"default null" json:"coin_symbol"`
	SharedDate *time.Time `gorm:"column:shared_date" sql:"default null" json:"shared_date"`
	Find       bool       `gorm:"column:find" sql:"default false" json:"find"`
}

type CoinTypePart struct {
	ID     int64  `gorm:"column:id; AUTO_INCREMENT; primary_key" sql:"not null; type:bigint unsigned" json:"id"`
	Symbol string `gorm:"column:symbol" sql:"not null; type:varchar(16)" json:"symbol"`
}

func updateLockCoinMap(db *gorm.DB, pairMap *sync.Map, isExit chan bool) error {
	var err error

	update := func() error {
		var coinStatus []TradeCoinStatus
		err = db.Raw("select coin_symbol, shared_date from token_lock_proj WHERE status = ? and UNIX_TIMESTAMP(list_date) <= ?", CoinTRADE, time.Now().Unix()).Scan(&coinStatus).Error
		if err != nil {
			return err
		}

		for index, item := range coinStatus {
			if item.SharedDate != nil {
				// if time.Now().Before(*item.SharedDate) || time.Now().Equal(*item.SharedDate) {
				// 	coinStatus[index].Find = true
				// }
				//test
				if time.Now().After(*item.SharedDate) || time.Now().Equal(*item.SharedDate) {
					coinStatus[index].Find = true
				}
			}
		}

		if len(coinStatus) > 0 {
			var coinSymbol []string
			for _, item := range coinStatus {
				if item.Find {
					coinSymbol = append(coinSymbol, *item.CoinSymbol)
				}
			}

			var coinType []CoinTypePart
			err = db.Raw("select id, symbol from coin_type WHERE symbol in (?)", coinSymbol).Scan(&coinType).Error
			if err != nil {
				return err
			}

			for _, it := range coinType {
				pairMap.Store(it.Symbol, it.ID)
			}
		}
		return nil
	}

	err = update()
	if err != nil {
		return err
	}

	go func() {
		ticker := time.NewTicker(time.Second * 5 * 60) //五分钟更新一次
		for {
			select {
			case <-ticker.C:
				err = update()
				if err != nil {
					//print log
				}
			case <-isExit:
				return
			}
		}
	}()

	return err
}
