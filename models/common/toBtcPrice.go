package modcom

import (
	"encoding/json"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/go-redis/redis"
)

//价格缓存时间 2分钟  milliseconds
const CACHE_TIMEOUT = 2 * 60

func coinPriceToBTC(rc *redis.Client, price2BTC *sync.Map, coinSymbol string) (float64, error) {
	//key {"pair":"BTC_USDT","time":1553147287941,"price":4000,"amount":0.59,"side":1,"id":2081577}
	pInfo, isExist := price2BTC.Load(coinSymbol)
	if coinSymbol == "BTC" {
		return 1, nil
	}

	if isExist {
		//milliseconds
		if priceInfo, ok := pInfo.(*LocPriceInfo); ok {
			if time.Now().Unix()*1000-priceInfo.UpdatedTime < CACHE_TIMEOUT {
				return priceInfo.Price, nil
			}
		}
	}

	//拉取最新价格
	var tickerKey string
	if coinSymbol == "USDT" || coinSymbol == "DAI" {
		tickerKey = strings.Join([]string{Prefix_ticker, "BTC_", coinSymbol}, "")
	} else {
		tickerKey = strings.Join([]string{Prefix_ticker, coinSymbol, "_BTC"}, "")
	}

	keyVal, err := rc.Get(tickerKey).Result()
	if err != nil {
		return 0, err
	}

	decoder := json.NewDecoder(strings.NewReader(keyVal))
	decoder.UseNumber()

	var priceInfo interface{}
	if err := decoder.Decode(&priceInfo); err != nil {
		return 0, err
	}

	priceMap := priceInfo.(map[string]interface{})
	price := priceMap["price"]
	var lastPrice float64

	if reflect.TypeOf(price).Name() == "Number" {
		lastPrice, err = price.(json.Number).Float64()
		if err != nil {
			return 0, err
		}
	} else if reflect.TypeOf(price).Name() == "string" {
		lastPrice, err = strconv.ParseFloat(price.(string), 64)
		if err != nil {
			return 0, err
		}
	}

	valPrice := &LocPriceInfo{time.Now().Unix(), lastPrice}
	price2BTC.Store(coinSymbol, valPrice)

	if lastPrice > 0 {
		lastPrice = 1 / lastPrice
	} else {
		lastPrice = 0
	}

	return lastPrice, nil
}
