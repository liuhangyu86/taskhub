package modcom

import (
	"errors"
	"taskhub/models/tables"

	"github.com/jinzhu/gorm"
)

func SysToUser(db *gorm.DB, tx *gorm.DB,
	relayId int64, userId int64,
	coinSymbol string, amount float64,
	SYS_USER_ID int, billType int, comment string) error {

	if db == nil {
		return errors.New("db is nul")
	}

	if tx == nil {
		tx := db.Begin()
		if err := tx.Error; err != nil {
			return err
		}
	}

	var coinInfo tables.CoinType
	err := db.Where("symbol = ? and is_active = ?", coinSymbol, 1).First(coinInfo).Error
	if err != nil {
		return err
	}

	//系统扣除
	{
		userAssets := new(tables.UserAssets)
		err := db.Where("user_id = ? and coin_id = ?", SYS_USER_ID, coinInfo.ID).First(userAssets).Error
		if err != nil {
			if gorm.IsRecordNotFoundError(err) {
				userAssets = nil
			} else {
				return err
			}
		}

		balance := 0 - amount
		if userAssets == nil { //create record
			userAssets.UserID = int64(SYS_USER_ID)
			userAssets.CoinID = int(coinInfo.ID)
			userAssets.CoinSymbol = coinInfo.Symbol
			userAssets.Balance = balance
			userAssets.Freeze = 0
			err := tx.Create(userAssets).Error
			if err != nil {
				return err
			}
		} else { //update record
			balance = userAssets.Balance + balance
			err := tx.Model(userAssets).Updates(map[string]interface{}{"balance": balance}).Error
			if err != nil {
				return err
			}
		}

		var userBills tables.UserBills
		userBills.UserID = int64(SYS_USER_ID)
		userBills.AccountType = new(int)
		*userBills.AccountType = ACCOUNT_FREEZE_TYPE_MAIN
		userBills.CoinID = coinInfo.ID
		userBills.CoinSymbol = coinInfo.Symbol
		userBills.BillType = int64(billType)
		userBills.ChangeAmount = balance + userAssets.Balance
		userBills.RelayID = float64(relayId)
		userBills.Comment = new(string)
		*userBills.Comment = comment
		err = tx.Create(userBills).Error
		if err != nil {
			return err
		}
	}

	//用户增加
	{
		userAssets := new(tables.UserAssets)
		err := db.Where("user_id = ? and coin_id = ?", SYS_USER_ID, coinInfo.ID).First(userAssets).Error
		if err != nil {
			if gorm.IsRecordNotFoundError(err) {
				userAssets = nil
			} else {
				return err
			}
		}

		balance := amount
		if userAssets == nil {
			userAssets.UserID = int64(userId)
			userAssets.CoinID = int(coinInfo.ID)
			userAssets.CoinSymbol = coinInfo.Symbol
			userAssets.Balance = balance
			userAssets.Freeze = 0
			err := tx.Create(userAssets).Error
			if err != nil {
				return err
			}
		} else {
			balance = userAssets.Balance + amount
			err := tx.Model(userAssets).Updates(map[string]interface{}{"balance": balance}).Error
			if err != nil {
				return err
			}
		}

		var userBills tables.UserBills
		userBills.UserID = userId
		userBills.AccountType = new(int)
		*userBills.AccountType = ACCOUNT_FREEZE_TYPE_MAIN
		userBills.CoinID = coinInfo.ID
		userBills.CoinSymbol = coinInfo.Symbol
		userBills.BillType = int64(billType)
		userBills.ChangeAmount = amount
		userBills.Fee = 0
		userBills.FeeSymbol = coinInfo.Symbol
		userBills.ResultAmount = balance + userAssets.Freeze
		userBills.RelayID = float64(relayId)
		userBills.Comment = new(string)
		*userBills.Comment = comment
		err = tx.Create(userBills).Error
		if err != nil {
			return err
		}
	}
	return nil
}
