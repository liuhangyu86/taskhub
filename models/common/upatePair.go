package modcom

import (
	"sync"
	"time"

	modtab "taskhub/models/tables"

	"github.com/jinzhu/gorm"
)

func updatePairMap(db *gorm.DB, pairMap *sync.Map, isExit chan bool) error {
	var err error

	queryExPairTab := func(db *gorm.DB) ([]modtab.ExPair, error) {
		var exPair []modtab.ExPair
		err = db.Where("pair_type = ?", 0).Find(&exPair).Error
		if err != nil {
			if gorm.IsRecordNotFoundError(err) {
			} else {
			}
			return nil, err
		}
		return exPair, nil
	}

	update := func() error {
		expair, err := queryExPairTab(db)
		if err != nil {
			return err
		}

		for _, item := range expair {
			pairMap.Store(item.Pair, 1)
		}
		return nil
	}

	err = update()
	if err != nil {
		return err
	}

	go func() {
		ticker := time.NewTicker(time.Second * 5 * 60) //五分钟更新一次
		for {
			select {
			case <-ticker.C:
				err = update()
				if err != nil {
					//print log
				}
			case <-isExit:
				//print log
				return
			}
		}
	}()

	return err
}
