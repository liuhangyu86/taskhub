package modcom

const (
	ACCOUNT_FREEZE_TYPE_MAIN     = 0 //现货账户
	ACCOUNT_FREEZE_TYPE_CREDIT   = 1 //信用账户
	ACCOUNT_FREEZE_TYPE_CONTRACT = 2 //合约账户
	ACCOUNT_FREEZE_TYPE_SYSTEM   = 3 //系统账户
	ACCOUNT_FREEZE_TYPE_CURRENCY = 4 //法币账户

	USER_BILLS_GAMES_SIGN_UP = 105 // 签到活动
)

const (
	NATIONDAY        = 0 // 国庆节
	NEW_YEAH         = 1 // 双蛋
	RECHARGE         = 2 // 是否有充值
	TRANSACTION      = 3 // 是否交易
	SIGN_UP          = 4 // 签到活动
	GAMES_TRADE_RANK = 5 // 春节排位赛活动
	GAMES_VALENTINE  = 6 // 情人节活动
)

type CoinStatus int

const (
	/**初始化，不可用状态 */
	CoinInit CoinStatus = 0
	/**锁仓状态 */
	CoinLOCK CoinStatus = 1
	/**交易状态 */
	CoinTRADE CoinStatus = 2
	/**项目锁仓失败  */
	CoinOFFLINE CoinStatus = 3
	/**预热 */
	CoinREADY CoinStatus = 4
)

// type PriceInfo struct {
// 	Pair string `json:"pair,omitempty"` //序列化的时候忽略0值或者空值
// 	Time int64  `json:"time,omitempty"`
// 	//Price  float64 `json:"price,omitempty"`
// 	Price  interface{} `json:"price,omitempty"` //redis Price数据类型string float64
// 	Amount float64     `json:"amount,omitempty"`
// 	Side   int         `json:"side,omitempty"`
// 	ID     int64       `json:"id,omitempty"`
// }

type LocPriceInfo struct {
	UpdatedTime int64   `json:"updatedTime"`
	Price       float64 `json:"price"`
	//Price decimal.Decimal `json:"price"`
}

//redis 常用key前缀
const (
	Prefix_ticker         = "bibox_key_ticker."              //价格行情, 如：bibox_key_ticker.BIX_BTC
	Prefix_orderbook      = "bibox_key_orderbook."           //深度表, 如: bibox_key_orderbook.BIX_BTC
	KEY_CNY_EXCHANGE_RATE = "CNY_EXCHANGE_RATE"              //人民币兑各国法币汇率
	SUFFIX_KEY_TICKER     = "_LAST_MARKET_TICKER"            // coinmarketcap 行情 BTC_LAST_MARKET_TICKER
	KEY_COINCAP_TICKER    = "_LAST_COINCAP_TICKER"           // coincap 行情 BTC_LAST_COINCAP_TICKER
	KEY_BINANCE_TICKER    = "_LAST_BINANCE_TICKER"           // binance 行情 BTCUSDT_LAST_BINANCE_TICKER
	KEY_BITFINEX_TICKER   = "_LAST_BITFINEX_TICKER"          // bitfinex 行情 BTCUSD_LAST_BITFINEX_TICKER
	KEY_OKEX_TICKER       = "_LAST_OKEX_TICKER"              // okex 行情 BTC_USDT_LAST_OKEX_TICKER
	KEY_HUOBI_TICKER      = "_LAST_HUOBI_TICKER"             // huobi 行情 btcusdt_LAST_HUOBI_TICKER
	CONTRACT_PRICE_LIMIT  = "bibox_key_contract_price_limit" // 合约价格限制
)
