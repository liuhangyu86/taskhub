package tables

import "time"

type CoinType struct {
	ID                  int64      `gorm:"column:id; AUTO_INCREMENT; primary_key" sql:"not null; type:bigint unsigned" json:"id"`
	Symbol              string     `gorm:"column:symbol" sql:"not null; type:varchar(16)" json:"symbol"`
	IsActive            int        `gorm:"column:is_active" sql:"not null; default:0; type:int unsigned" json:"is_active"`
	Name                string     `gorm:"column:name" sql:"not null; type:varchar(45)" json:"name"`
	OriginalDecimals    int        `gorm:"column:original_decimals" sql:"not null; default:18; type:int unsigned" json:"original_decimals"`
	ValidDecimals       int        `gorm:"column:valid_decimals" sql:"not null; default:8; type:int unsigned" json:"valid_decimals"`
	IsErc20             int        `gorm:"column:is_erc20" sql:"not null; default:0; type:int unsigned" json:"is_erc20"`
	Contract            *string    `gorm:"column:contract" sql:"default null; type:varchar(100)" json:"contract"`
	ContractFather      *string    `gorm:"column:contract_father" sql:"default null; type:varchar(16)" json:"contract_father"`
	EnableFeposit       int        `gorm:"column:enable_deposit" sql:"not null; default:0; type:int unsigned" json:"enable_deposit"`
	EnableWithdraw      int        `gorm:"column:enable_withdraw" sql:"not null; default:0; type:int unsigned" json:"enable_withdraw"`
	DepositConfirmCount int        `gorm:"column:deposit_confirm_count" sql:"not null; default:0; type:int unsigned" json:"deposit_confirm_count"`
	WithdrawFee         float64    `gorm:"column:withdraw_fee" sql:"type:decimal(24,8); not null; default:0.00000000" json:"withdraw_fee"`
	WithdrawMin         float64    `gorm:"column:withdraw_min" sql:"type:decimal(24,8); not null; default:0.00000000" json:"withdraw_min"`
	ExplorURL           string     `gorm:"column:explor_url" sql:"default ''" json:"explor_url"`
	IconURL             *string    `gorm:"column:icon_url" sql:"default null" json:"icon_url"`
	DescribeURL         *string    `gorm:"column:describe_url" sql:"default null" json:"describe_url"`
	SafeConfirmCount    int        `gorm:"column:safe_confirm_count" sql:"not null; type:int unsigned" json:"safe_confirm_count"`
	Weight              int        `gorm:"column:weight" sql:"not null; default:0; type:int unsigned" json:"weight"`
	ForbidInfo          string     `gorm:"column:forbid_info" sql:"default ''" json:"forbid_info"`
	DescribeSummary     string     `gorm:"column:describe_summary" sql:"default ''" json:"describe_summary"`
	TotalAmount         float64    `gorm:"column:total_amount" sql:"type:decimal(24,8); not null; default:0.00000000" json:"total_amount"`
	SupplyAmount        float64    `gorm:"column:supply_amount" sql:"type:decimal(24,8); not null; default:0.00000000" json:"supply_amount"`
	Price               string     `gorm:"column:price" sql:"default '0'" json:"price"`
	SupplyTime          string     `gorm:"column:supply_time" sql:"default '0'" json:"supply_time"`
	Comment             JSON       `gorm:"column:comment" sql:"default null;type:json" json:"comment"`
	CreatedAt           *time.Time `gorm:"column:createdAt" sql:"default null" json:"createdAt"`
	UpdatedAt           *time.Time `gorm:"column:updatedAt" sql:"default null" json:"updatedAt"`
}
