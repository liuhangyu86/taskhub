package tables

import (
	"time"
)

type UserBills struct {
	ID           int64      `gorm:"column:id; AUTO_INCREMENT; primary_key" sql:"not null; type:bigint unsigned" json:"id"`
	UserID       int64      `gorm:"column:user_id" sql:"not null; default:'0'; type:int unsigned" json:"user_id"`
	AccountType  *int       `gorm:"column:account_type" sql:"default:nill; type:int unsigned" json:"account_type"`
	CoinID       int64      `gorm:"column:coin_id" sql:"not null; default:0; type:int unsigned" json:"coin_id"`
	CoinSymbol   string     `gorm:"column:coin_symbol" sql:"default ''" json:"coin_symbol"`
	BillType     int64      `gorm:"column:bill_type" sql:"not null; default:'0'; type:int unsigned" json:"bill_type"`
	ChangeAmount float64    `gorm:"column:change_amount" sql:"type:decimal(34,10); not null; default:0.0000000000" json:"change_amount"`
	Fee          float64    `gorm:"column:fee" sql:"type:decimal(24,10); not null; default:0.0000000000" json:"fee"`
	FeeSymbol    string     `gorm:"column:fee_symbol" sql:"default ''" json:"fee_symbol"`
	ResultAmount float64    `gorm:"column:result_amount" sql:"type:decimal(34,10); not null; default:0.0000000000" json:"result_amount"`
	RelayID      float64    `gorm:"column:relay_id" sql:"type:decimal(20,0); not null; default:0" json:"relay_id"`
	Comment      *string    `gorm:"column:comment" sql:"default null" json:"comment"`
	SerialNumber int64      `gorm:"column:serial_number" sql:"not null; default:'0'; type:int unsigned" json:"serial_number"`
	CreatedAt    *time.Time `gorm:"column:createdAt" sql:"default null" json:"createdAt"`
	UpdatedAt    *time.Time `gorm:"column:updatedAt" sql:"default null" json:"updatedAt"`
}
