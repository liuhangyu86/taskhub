package tables

import "time"

type TokenLockProj struct {
	ID            int64      `gorm:"column:id; AUTO_INCREMENT; primary_key" sql:"not null; type:bigint unsigned" json:"id"`
	CoinSymbol    *string    `gorm:"column:coin_symbol" sql:"default null" json:"coin_symbol"`
	Status        int        `gorm:"column:status" sql:"not null; default:0; type:int unsigned" json:"status"`
	ProjInitFund  float64    `gorm:"column:proj_init_fund" sql:"type:decimal(32,10); not null; default:0.0000000000" json:"proj_init_fund"`
	ProjCulFund   float64    `gorm:"column:proj_cul_fund" sql:"type:decimal(32,10); not null; default:0.0000000000" json:"proj_cul_fund"`
	Amount        float64    `gorm:"column:amount" sql:"type:decimal(32,10); not null; default:0.0000000000" json:"amount"`
	LockedAmount  float64    `gorm:"column:locked_amount" sql:"type:decimal(32,10); not null; default:0.0000000000" json:"locked_amount"`
	LockBegin     *time.Time `gorm:"column:lock_begin" sql:"default null" json:"lock_begin"`
	LockEnd       *time.Time `gorm:"column:lock_end" sql:"default null" json:"lock_end"`
	ListDate      *time.Time `gorm:"column:list_date" sql:"default null" json:"list_date"`
	RemoveDate    *time.Time `gorm:"column:remove_date" sql:"default null" json:"remove_date"`
	LockMin       float64    `gorm:"column:proj_inilock_mint_fund" sql:"type:decimal(32,10); not null; default:0.0000000000" json:"lock_min"`
	LockMax       float64    `gorm:"column:lock_max" sql:"type:decimal(32,10); not null; default:0.0000000000" json:"lock_max"`
	UnlockDate    *time.Time `gorm:"column:unlock_date" sql:"default null" json:"unlock_date"`
	SharedDate    *time.Time `gorm:"column:shared_date" sql:"default null" json:"shared_date"`
	ShareEnd      *time.Time `gorm:"column:share_end" sql:"default null" json:"share_end"`
	ShareStatus   int        `gorm:"column:share_status" sql:"not null; default:0; type:int unsigned" json:"share_status"`
	Info          string     `gorm:"column:info" sql:"type:mediumtext" json:"info"`
	Comment       *string    `gorm:"column:comment" sql:"default null" json:"comment"`
	Introducer    *int64     `gorm:"column:introducer" sql:"default null; type:bigint unsigned" json:"introducer"`
	IntroduceRate *float64   `gorm:"column:introduce_rate" sql:"type:decimal(32,10);default null" json:"introduce_rate"`
	CreatedAt     time.Time  `gorm:"column:createdAt" sql:"not null" json:"createdAt"`
	UpdatedAt     time.Time  `gorm:"column:updatedAt" sql:"not null" json:"updatedAt"`
}
