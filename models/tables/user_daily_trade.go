package tables

import "time"

type UserDailyTrade struct {
	ID                 int64     `gorm:"column:id; AUTO_INCREMENT; primary_key" sql:"not null; type:bigint unsigned" json:"id"`
	UserID             int64     `gorm:"column:user_id" sql:"not null; type:bigint unsigned" json:"user_id"`
	TradeDate          time.Time `gorm:"column:trade_date" sql:"not null" json:"trade_date"`
	EqualBtc           float64   `gorm:"column:equal_btc" sql:"type:decimal(24,8); not null; default:0.00000000" json:"equal_btc"`
	EqualUsdt          float64   `gorm:"column:equal_usdt" sql:"type:decimal(24,8); not null; default:0.00000000" json:"equal_usdt"`
	EqualBtcFree       float64   `gorm:"column:equal_btc_free" sql:"type:decimal(24,8); not null; default:0.00000000" json:"equal_btc_free"`
	EqualUsdtFree      float64   `gorm:"column:equal_usdt_free" sql:"type:decimal(24,8); not null; default:0.00000000" json:"equal_usdt_free"`
	EqualBtcMine       float64   `gorm:"column:equal_btc_mine" sql:"type:decimal(24,8); not null; default:0.00000000" json:"equal_btc_mine"`
	EqualUsdtMine      float64   `gorm:"column:equal_usdt_mine" sql:"type:decimal(24,8); not null; default:0.00000000" json:"equal_usdt_mine"`
	EqualBtcMineFree   float64   `gorm:"column:equal_btc_mine_free" sql:"type:decimal(24,8); not null; default:0.00000000" json:"equal_btc_mine_free"`
	EqualUsdtMineFree  float64   `gorm:"column:equal_usdt_mine_free" sql:"type:decimal(24,8); not null; default:0.00000000" json:"equal_usdt_mine_free"`
	EqualBtcShare      float64   `gorm:"column:equal_btc_share" sql:"type:decimal(24,8); not null; default:0.00000000" json:"equal_btc_share"`
	EqualUsdtShare     float64   `gorm:"column:equal_usdt_share" sql:"type:decimal(24,8); not null; default:0.00000000" json:"equal_usdt_share"`
	EqualBtcShareFree  float64   `gorm:"column:equal_btc_share_free" sql:"type:decimal(24,8); not null; default:0.00000000" json:"equal_btc_share_free"`
	EqualUsdtShareFree float64   `gorm:"column:equal_usdt_share_free" sql:"type:decimal(24,8); not null; default:0.00000000" json:"equal_usdt_share_free"`
	Status             int       `gorm:"column:status" sql:"not null; default:0; type:int unsigned" json:"status"`
	CreatedAt          time.Time `gorm:"column:createdAt" sql:"not null" json:"createdAt"`
	UpdatedAt          time.Time `gorm:"column:updatedAt" sql:"not null" json:"updatedAt"`
}
