package tables

import "time"

type GamesRaiseflag struct {
	ID           int64     `gorm:"column:id; AUTO_INCREMENT; primary_key" sql:"not null; type:bigint unsigned" json:"id"` //omitempty 不能忽略
	Number       int       `gorm:"column:number" sql:"not null; default:0; type:int unsigned" json:"number"`
	Type         int       `gorm:"column:type" sql:"not null; type:int unsigned" json:"type"`
	PrizeMode    int       `gorm:"column:prize_mode" sql:"not null; default:0; type:int unsigned" json:"prize_mode"`
	BeginTime    time.Time `gorm:"column:begin_time" sql:"not null" json:"begin_time"`
	EndTime      time.Time `gorm:"column:end_time" sql:"not null" json:"end_time"`
	LastShowTime time.Time `db:"last_show_time" sql:"null" json:"last_show_time"`
	Active       int       `gorm:"column:active" sql:"not null; default:0; type:int unsigned" json:"active"`
	CreatedAt    time.Time `gorm:"column:createdAt" sql:"not null" json:"createdAt"`
	UpdatedAt    time.Time `gorm:"column:updatedAt" sql:"not null" json:"updatedAt"`
	Desc         *string   `gorm:"column:desc" sql:"default null" json:"desc"`
}
