package tables

import "time"

type ExTradeDetail struct {
	ID             int64     `gorm:"column:id; AUTO_INCREMENT; primary_key" sql:"not null; type:bigint unsigned" json:"id"`
	UserID         int64     `gorm:"column:user_id" sql:"not null; type:bigint unsigned" json:"user_id"`
	AccountType    int       `gorm:"column:account_type" sql:"not null; default:0; type:int unsigned" json:"account_type"`
	Pair           string    `gorm:"column:pair" sql:"default ''" json:"pair"`
	CoinID         int       `gorm:"column:coin_id" sql:"not null; type:int unsigned" json:"coin_id"`
	CoinSymbol     string    `gorm:"column:coin_symbol" sql:"default ''" json:"coin_symbol"`
	CurrencyID     int       `gorm:"column:currency_id" sql:"not null; default:1; type:int unsigned" json:"currency_id"`
	CurrencySymbol string    `gorm:"column:currency_symbol" sql:"default 'BTC'" json:"currency_symbol"`
	OrderSide      int       `gorm:"column:order_side" sql:"not null; type:int unsigned" json:"order_side"`
	OrderType      int       `gorm:"column:order_type" sql:"not null; type:int unsigned" json:"order_type"`
	IsMaker        int       `gorm:"column:is_maker" sql:"not null; default:0; type:int unsigned" json:"is_maker"`
	Price          float64   `gorm:"column:price" sql:"type:decimal(24,8); not null; default:0.00000000" json:"price"`
	Amount         float64   `gorm:"column:amount" sql:"type:decimal(32,8); not null; default:0.00000000" json:"amount"`
	Money          float64   `gorm:"column:money" sql:"type:decimal(32,8); not null; default:0.00000000" json:"money"`
	DealPrice      float64   `gorm:"column:deal_price" sql:"type:decimal(32,14); not null; default:0.00000000000000" json:"deal_price"`
	DealAmount     float64   `gorm:"column:deal_amount" sql:"type:decimal(32,14); not null; default:0.00000000000000" json:"deal_amount"`
	DealMoney      float64   `gorm:"column:deal_money" sql:"type:decimal(32,14); not null; default:0.00000000000000" json:"deal_money"`
	RelayID        int64     `gorm:"column:relay_id" sql:"not null; type:bigint unsigned" json:"relay_id"`
	FeeRate        float64   `gorm:"column:fee_rate" sql:"type:decimal(24,8); not null; default:0.00000000" json:"fee_rate"`
	Fee            float64   `gorm:"column:fee" sql:"type:decimal(24,8); not null; default:0.00000000" json:"fee"`
	PayBix         int       `gorm:"column:pay_bix" sql:"not null; default:0; type:int unsigned" json:"pay_bix"`
	IsPersisted    int       `gorm:"column:is_persisted" sql:"not null; default:0; type:int unsigned" json:"is_persisted"`
	CreatedAt      time.Time `gorm:"column:createdAt" sql:"not null" json:"createdAt"`
	UpdatedAt      time.Time `gorm:"column:updatedAt" sql:"not null" json:"updatedAt"`
}
