package tables

import "time"

type GamesUserSignUp struct {
	ID          int64     `gorm:"column:id; AUTO_INCREMENT; primary_key" sql:"not null; type:bigint unsigned" json:"id"`
	UserID      int64     `gorm:"column:user_id" sql:"not null; type:bigint unsigned" json:"user_id"`
	Date        time.Time `gorm:"column:date" sql:"not null" json:"date"`
	Amount      float64   `gorm:"column:amount" sql:"type:decimal(32,10); not null; default:0.0000000000" json:"amount"`
	ShareAmount float64   `gorm:"column:share_amount" sql:"type:decimal(32,10); default:0.0000000000" json:"share_amount"`
	IsSignUp    int       `gorm:"column:is_sign_up" sql:"not null; default:0; type:int unsigned" json:"is_sign_up"`
	IsSignIn    int       `gorm:"column:is_sign_in" sql:"not null; default:0; type:int unsigned" json:"is_sign_in"`
	IsDraw      int       `gorm:"column:is_draw" sql:"not null; default:0; type:int unsigned" json:"is_draw"`
	ContiDays   int       `gorm:"column:conti_days" sql:"not null; default:0; type:int unsigned" json:"conti_days"`
	IsContinous int       `gorm:"column:is_continous" sql:"not null; default:0; type:int unsigned" json:"is_continous"`
	Comment     *string   `gorm:"column:comment" sql:"default null" json:"comment"`
	CreatedAt   time.Time `gorm:"column:createdAt" sql:"not null" json:"createdAt"`
	UpdatedAt   time.Time `gorm:"column:updatedAt" sql:"not null" json:"updatedAt"`
}
