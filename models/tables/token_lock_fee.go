package tables

import "time"

type TokenLockFee struct {
	ID             int64      `gorm:"column:id; AUTO_INCREMENT; primary_key" sql:"not null; type:bigint unsigned" json:"id"`
	UserID         *int64     `gorm:"column:user_id" sql:"default null" json:"user_id"`
	TradeDate      *time.Time `gorm:"column:trade_date" sql:"default null" json:"trade_date"`
	FeeSymbol      string     `gorm:"column:fee_symbol" sql:"not null; default ''" json:"fee_symbol"`
	CoinSymbol     string     `gorm:"column:coin_symbol" sql:"not null; default ''" json:"coin_symbol"`
	CurrencySymbol string     `gorm:"column:currency_symbol" sql:"not null" json:"currency_symbol"`
	UpdatedAt      time.Time  `gorm:"column:updatedAt" sql:"not null" json:"updatedAt"`
	DealAmount     *float64   `gorm:"column:deal_amount" sql:"type:decimal(34,10);default:null" json:"deal_amount"`
	DealAmountBtc  *float64   `gorm:"column:deal_amount_btc" sql:"type:decimal(34,10);default:null" json:"deal_amount_btc"`
	DealAmountUsdt *float64   `gorm:"column:deal_amount_usdt" sql:"type:decimal(34,10);default:null" json:"deal_amount_usdt"`
	EqualBtc       *float64   `gorm:"column:equal_btc" sql:"type:decimal(34,10);default:null" json:"equal_btc"`
	EqualUsdt      *float64   `gorm:"column:equal_usdt" sql:"type:decimal(34,10);default:null" json:"equal_usdt"`
	DeeAmount      *float64   `gorm:"column:fee_amount" sql:"type:decimal(34,10);default:null" json:"fee_amount"`
	CreatedAt      time.Time  `gorm:"column:createdAt" sql:"not null" json:"createdAt"`
}
