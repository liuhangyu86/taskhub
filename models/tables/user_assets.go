package tables

import (
	"time"
)

type UserAssets struct {
	ID         int64      `gorm:"column:id; AUTO_INCREMENT; primary_key" sql:"not null; type:bigint unsigned" json:"id"`
	UserID     int64      `gorm:"column:user_id" sql:"not null; default:0; type:int unsigned" json:"user_id"`
	CoinID     int        `gorm:"column:coin_id" sql:"not null; default:0; type:int unsigned" json:"coin_id"`
	CoinSymbol string     `gorm:"column:coin_symbol" sql:"default ''" json:"coin_symbol"`
	Balance    float64    `gorm:"column:balance" sql:"type:decimal(34,10); not null; default:0.0000000000" json:"balance"`
	Freeze     float64    `gorm:"column:freeze" sql:"type:decimal(34,10); not null; default:0.0000000000" json:"freeze"`
	CreatedAt  *time.Time `gorm:"column:createdAt" sql:"default null" json:"createdAt"`
	UpdatedAt  *time.Time `gorm:"column:updatedAt" sql:"default null" json:"updatedAt"`
}
