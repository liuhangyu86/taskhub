package tables

import "time"

type ExPair struct {
	ID         int64     `gorm:"column:id; AUTO_INCREMENT; primary_key" sql:"not null; type:bigint unsigned" json:"id"`
	Pair       string    `gorm:"column:pair" sql:"default ''" json:"pair"`
	PairType   int       `gorm:"column:pair_type" sql:"not null; default:0; type:int unsigned" json:"pair_type"`
	IsActive   int       `gorm:"column:is_active" sql:"not null; default:0; type:int unsigned" json:"is_active"`
	CoinID     int       `gorm:"column:coin_id" sql:"not null; type:int unsigned" json:"coin_id"`
	CurrencyID int       `gorm:"column:currency_id" sql:"not null; default:0; type:int unsigned" json:"currency_id"`
	MakerRate  float64   `gorm:"column:maker_rate" sql:"type:decimal(10,8); not null; default:0.00000000" json:"maker_rate"`
	TakerRate  float64   `gorm:"column:taker_rate" sql:"type:decimal(10,8); not null; default:0.00000000" json:"taker_rate"`
	IsIndex    int       `gorm:"column:is_index" sql:"not null; default:0; type:int unsigned" json:"is_index"`
	IsHide     int       `gorm:"column:is_hide" sql:"not null; default:0; type:int unsigned" json:"is_hide"`
	AreaID     int       `gorm:"column:area_id" sql:"not null; default:7; type:int unsigned" json:"area_id"`
	CreatedAt  time.Time `gorm:"column:createdAt" sql:"not null" json:"createdAt"`
	UpdatedAt  time.Time `gorm:"column:updatedAt" sql:"not null" json:"updatedAt"`
}
