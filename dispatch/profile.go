package dispatch

import (
	"net"
	"net/http"
	"net/http/pprof"
	"os"
	"strconv"

	mux "github.com/gorilla/mux"
)

const (
	ServerProfilePort  = "SERVER_PROFILE_PORT"
	ProfileAddrDefault = "127.0.0.1"
	ProfilePortDefault = "8787"
)

func checkAndEnableProfiling() error {
	pport := os.Getenv(ServerProfilePort)
	if pport != "" {
		iport, err := strconv.Atoi(pport)
		if err != nil || iport < 0 {
			logger.Errorf("Profile port specified by the %s environment variable is not a valid port, not enabling profiling",
				ServerProfilePort)
		} else {
			addr := net.JoinHostPort(ProfileAddrDefault, ProfilePortDefault)
			listener, err1 := net.Listen("tcp", addr)
			logger.Infof("Profiling enabled; listening for profile requests on port %s", pport)
			if err1 != nil {
				return err1
			}
			go func() {
				r := mux.NewRouter()
				r.HandleFunc("/debug/pprof/", pprof.Index)
				r.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
				r.HandleFunc("/debug/pprof/profile", pprof.Profile)
				r.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
				r.HandleFunc("/debug/pprof/trace", pprof.Trace)

				logger.Debugf("Profiling enabled; waiting for profile requests on port %s", pport)
				http.Serve(listener, nil)
				logger.Errorf("Stopped serving for profiling requests on port %s: %s", pport, err)
			}()
		}
	}
	return nil
}
