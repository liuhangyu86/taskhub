package dispatch

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"io/ioutil"
	"regexp"
	"strings"
	"time"

	"github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

type SqlxDB struct {
	db              *sqlx.DB
	IsDBInitialized bool
	dbName          string
}

func (tasks *TaskProcess) OpenSqlxIns() error {
	dbase := tasks.appConfig.DataBase
	ds := dbase.DataSource
	ds = MaskDBCred(ds)
	logger.Debugf("Initializing Sqlx '%s' database at '%s'", dbase.Type, ds)

	switch dbase.Type {
	case "mysql":
		dbName := getDBName(dbase.DataSource)
		logger.Debugf("Database Name: %s", dbName)

		re := regexp.MustCompile(`\/([0-9,a-z,A-Z$_]+)`)
		connStr := re.ReplaceAllString(dbase.DataSource, "/")

		if dbase.TLS.Enabled {
			tlsConfig, err := GetClientTLSConfig(dbase.TLS.CaCert, dbase.TLS.KeyFile, dbase.TLS.CertFile)
			if err != nil {
				return errors.New(fmt.Sprintf("Failed to get client TLS for MySQL %s", err.Error()))
			}
			mysql.RegisterTLSConfig("custom", tlsConfig)
		}

		logger.Debugf("Connecting to MySQL server, using connection string: %s", MaskDBCred(connStr))
		db, err := sqlx.Open("mysql", connStr)
		if err != nil {
			return errors.New(fmt.Sprintf("Failed to open MySQL database %s", err.Error()))
		}

		tasks.sqlxdb = &SqlxDB{db: db, IsDBInitialized: true, dbName: dbName}

		for i := 0; i <= 9; i++ {
			err = db.Ping()
			if err != nil {
				logger.Debugf("Failed to connect to MySQL database %s, waiting second %d reconnect...", err.Error(), i+1)
				time.Sleep(time.Second * time.Duration(i+1))
			} else {
				break
			}
		}
		err = db.Ping()
		if err != nil {
			return errors.New(fmt.Sprintf("Failed to connect to MySQL database %s", err.Error()))
		}

		db.SetMaxIdleConns(dbase.MaxIdle)
		db.SetMaxOpenConns(dbase.MaxOpen)
		// drop database temporarily
		//err = dropMySQLDatabase(dbName, db)
		err = createMySQLDatabase(dbName, db)
		if err != nil {
			return errors.New(fmt.Sprintf("Failed to create MySQL database %s", err.Error()))
		}

		logger.Debugf("Connecting to database '%s', using connection string: '%s'", dbName, MaskDBCred(dbase.DataSource))
		db, err = sqlx.Open("mysql", dbase.DataSource)
		if err != nil {
			return errors.New(fmt.Sprintf("Failed to open database (%s) in MySQL server %s", dbName, err.Error()))
		}

		err = createMySQLTables(dbName, db)
		if err != nil {
			return errors.New(fmt.Sprintf("Failed to create MySQL tables %s", err.Error()))
		}

		tasks.sqlxdb.IsDBInitialized = true
	default:
		return fmt.Errorf("Invalid db.type in config file: '%s'; must be  'tidb', or 'mysql'", dbase.Type)
	}

	logger.Infof("Initialized success %s database at %s ", dbase.Type, ds)
	return nil
}

func (tasks *TaskProcess) CloseSqlxIns() error {
	if tasks.sqlxdb != nil && tasks.sqlxdb.IsDBInitialized {
		err := tasks.sqlxdb.db.Close()
		if err != nil {
			return err
		}
		tasks.sqlxdb = nil
		return nil
	}
	return errors.New("sqlx handle not initialized")
}

func (tasks *TaskProcess) GetSqlxIns() (*sqlx.DB, string, error) {
	if tasks.sqlxdb != nil && tasks.sqlxdb.IsDBInitialized {
		return tasks.sqlxdb.db, tasks.sqlxdb.dbName, nil
	}
	return nil, "", errors.New("sqlx handle not initialized")
}

func getDBName(datasource string) string {
	var dbName string
	datasource = strings.ToLower(datasource)

	re := regexp.MustCompile(`(?:\/([^\/?]+))|(?:dbname=([^\s]+))`)
	getName := re.FindStringSubmatch(datasource)
	if getName != nil {
		dbName = getName[1]
		if dbName == "" {
			dbName = getName[2]
		}
	}

	return dbName
}

func GetX509CertificateFromPEM(cert []byte) (*x509.Certificate, error) {
	block, _ := pem.Decode(cert)
	if block == nil {
		return nil, errors.New("Failed to PEM decode certificate")
	}
	x509Cert, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("Error parsing certificate %s", err.Error()))
	}
	return x509Cert, nil
}

func checkCertDates(certFile string) error {
	logger.Debug("Check client TLS certificate for valid dates")
	certPEM, err := ioutil.ReadFile(certFile)
	if err != nil {
		return errors.New(fmt.Sprintf("Failed to read file '%s' %s", certFile, err.Error()))
	}

	cert, err := GetX509CertificateFromPEM(certPEM)
	if err != nil {
		return err
	}

	notAfter := cert.NotAfter
	currentTime := time.Now().UTC()

	if currentTime.After(notAfter) {
		return errors.New("Certificate provided has expired")
	}

	notBefore := cert.NotBefore
	if currentTime.Before(notBefore) {
		return errors.New("Certificate provided not valid until later date")
	}

	return nil
}

func GetClientTLSConfig(cacert string, key string, cert string) (*tls.Config, error) {
	var certs []tls.Certificate

	if cert != "" {
		err := checkCertDates(cert)
		if err != nil {
			return nil, err
		}

		clientCert, err := tls.LoadX509KeyPair(cert, key)
		if err != nil {
			return nil, err
		}

		certs = append(certs, clientCert)
	} else {
		logger.Debug("Client TLS certificate and/or key file not provided")
	}
	rootCAPool := x509.NewCertPool()
	if len(cacert) == 0 {
		return nil, errors.New("No trusted root certificates for TLS were provided")
	}

	caCert, err := ioutil.ReadFile(cacert)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("Failed to read '%s' %s", cacert, err.Error()))
	}
	ok := rootCAPool.AppendCertsFromPEM(caCert)
	if !ok {
		return nil, errors.New(fmt.Sprintf("Failed to process certificate from file %s %s", cacert, err.Error()))
	}

	config := &tls.Config{
		Certificates: certs,
		RootCAs:      rootCAPool,
	}

	return config, nil
}

func createMySQLDatabase(dbName string, db *sqlx.DB) error {
	logger.Debugf("Creating MySQL Database (%s) if it does not exist...", dbName)

	_, err := db.Exec("CREATE DATABASE IF NOT EXISTS " + dbName)
	if err != nil {
		return errors.New(fmt.Sprintf("Failed to execute create database query %s", err.Error()))
	}

	return nil
}

func createMySQLTables(dbName string, db *sqlx.DB) error {
	// var err error
	// logger.Debug("Creating test101 table if it doesn't exist")
	// if _, err := db.Exec("CREATE TABLE IF NOT EXISTS test101 (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,openid VARCHAR(80),pwd VARCHAR(80),phone VARCHAR(20),UNIQUE INDEX indexphone (phone)) DEFAULT CHARSET=utf8 COLLATE utf8_bin"); err != nil {
	// 	return errors.New(fmt.Sprintf("Error creating test101 table %s", err.Error()))
	// }

	// var err error
	// logger.Debug("Creating users table if it doesn't exist")
	// if _, err := db.Exec("CREATE TABLE IF NOT EXISTS users (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,openid VARCHAR(80),pwd VARCHAR(80),phone VARCHAR(20),UNIQUE INDEX indexphone (phone)) DEFAULT CHARSET=utf8 COLLATE utf8_bin"); err != nil {
	// 	return errors.New(fmt.Sprintf("Error creating users table %s", err.Error()))
	// }

	// logger.Debug("Creating upoint table if it doesn't exist")
	// if _, err := db.Exec("CREATE TABLE IF NOT EXISTS upoint (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,phone VARCHAR(20),daytime BIGINT UNSIGNED,tag VARCHAR(50),point VARCHAR(30),INDEX indexphone (phone), INDEX indextime (daytime)) DEFAULT CHARSET=utf8 COLLATE utf8_bin"); err != nil {
	// 	return errors.New(fmt.Sprintf("Error creating upoint table %s", err.Error()))
	// }

	// logger.Debug("Creating word table if it doesn't exist")
	// if _, err := db.Exec("CREATE TABLE IF NOT EXISTS word (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,rand VARCHAR(20),second BIGINT UNSIGNED,INDEX indexrand (rand)) DEFAULT CHARSET=utf8 COLLATE utf8_bin"); err != nil {
	// 	return errors.New(fmt.Sprintf("Error creating word table %s", err.Error()))
	// }

	// 	if _, err = db.Exec(
	// 		`CREATE TABLE users(
	// 			id BIGINT UNSIGNED NOT NULL,
	// 			createdat DATETIME(3) NOT NULL,
	// 			updatedat DATETIME(3) NOT NULL,
	// 			deletedat DATETIME(3) ,
	// 			state INT DEFAULT 0,
	// 			access JSON,
	// 			lastseen DATETIME,
	// 			public JSON,
	// -- 			tags JSON,
	// 			PRIMARY KEY(id)
	// 		)`); err != nil {
	// 		return errors.New("Error creating user table" + err.Error())
	// 	}

	return nil
}

func dropMySQLDatabase(dbName string, db *sqlx.DB) error {
	logger.Debugf("DROP MySQL Database (%s) if it does not exist...", dbName)

	_, err := db.Exec("DROP DATABASE  " + dbName)
	if err != nil {
		return errors.New(fmt.Sprintf("Failed to execute create database query %s", err.Error()))
	}

	return nil
}
