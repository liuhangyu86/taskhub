package dispatch

import (
	"sync"
	job3 "taskhub/service/share/job3"
	signupshare "taskhub/service/share/signUpShare"
	job4 "taskhub/service/statistics/job4"
	trademonitor "taskhub/service/statistics/tradeMonitor"

	"github.com/robfig/cron"
)

type Status struct {
	isRuning bool //表示是否已在运行
	sync.Mutex
}

func (s *Status) IsRuning() bool {
	if s == nil {
		return true
	}

	s.Lock()
	defer s.Unlock()
	if s.isRuning == false {
		s.isRuning = true
		return false
	}
	return true
}

func (s *Status) SetNoRuning() bool {
	if s == nil {
		return false
	}

	s.Lock()
	defer s.Unlock()
	if s.isRuning == true {
		s.isRuning = false
		return true
	}
	return false
}

type Loops struct {
	Cron *cron.Cron
}

func (tasks *TaskProcess) InitLoops() error {
	tasks.loops = &Loops{Cron: cron.New()}
	return nil
}

func (tasks *TaskProcess) StopLoops() error {
	if tasks.loops != nil {
		tasks.loops.Cron.Stop()
		tasks.loops = nil
	}
	return nil
}

func (tasks *TaskProcess) loopsAddTask(spect string, f func()) {
	if tasks.loops != nil {
		tasks.loops.Cron.AddFunc(spect, f)
	}
}

func (tasks *TaskProcess) LoopsRun() error {
	if tasks.loops != nil {
		spec := "* * */23 * * ?"
		tasks.loopsAddTask(spec, func() {
			logger.Debugf("%s", "system idle loop task....")
		})

		for i := 0; i < len(tasks.appConfig.Loops); i++ {
			item := tasks.appConfig.Loops[i]
			if item.Name == "signUpShare" {
				job1 := signupshare.SignUpShareJob{Index: i, TaskPro: tasks, Stats: nil}
				err := tasks.loops.Cron.AddJob(item.Cron, &job1)
				if err != nil {
					logger.Debugf("tasks manager add job1 err: %s", err.Error())
					return err
				}
				go job1.Run()
			} else if item.Name == "tradeMonitor" {
				stats := &Status{isRuning: false}
				job2 := trademonitor.TradeMonitorJob{Index: i, TaskPro: tasks, Stats: stats}
				err := tasks.loops.Cron.AddJob(item.Cron, &job2)
				if err != nil {
					logger.Debugf("tasks manager add job2 err: %s", err.Error())
					return err
				}
				go job2.Run()
			} else if item.Name == "job3" {
				job3 := job3.JobLoop3{Index: i, TaskPro: tasks, Stats: nil}
				err := tasks.loops.Cron.AddJob(item.Cron, &job3)
				if err != nil {
					logger.Debugf("tasks manager add job3 err: %s", err.Error())
					return err
				}
				go job3.Run()
			} else if item.Name == "job4" {
				job4 := job4.JobLoop4{Index: i, TaskPro: tasks, Stats: nil}
				err := tasks.loops.Cron.AddJob(item.Cron, &job4)
				if err != nil {
					logger.Debugf("tasks manager add job3 err: %s", err.Error())
					return err
				}
				go job4.Run()
			}
		}

		tasks.loops.Cron.Start() //任务必须先设置 然后在start
	}

	return nil
}
