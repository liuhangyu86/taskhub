package dispatch

import (
	"log"
	"time"

	coinPair "taskhub/service/timer/coinPair"
	coinPrice "taskhub/service/timer/coinPrice"
	lockCoin "taskhub/service/timer/lockCoin"

	"github.com/sankalpjonn/go-timeloop"
)

type Timer struct {
	timeloop *timeloop.Timeloop
}

func (tasks *TaskProcess) InitTimer() error {
	tasks.timer = &Timer{timeloop: timeloop.New()}
	return nil
}

func (tasks *TaskProcess) StopTimer() error {
	if tasks.timer != nil {
		tasks.timer.timeloop.Stop()
		tasks.timer = nil
	}
	return nil
}

func (tasks *TaskProcess) TimerRun() error {
	if tasks.timer != nil {
		tasks.timerAddTask(func(index int) {
			log.Printf("%s", "system idle timer task....")
		}, -1, time.Duration(60*60*24*time.Second))

		for i := 0; i < len(tasks.appConfig.Timers); i++ {
			item := tasks.appConfig.Timers[i]
			if item.Name == "coinPair" {
				stats := &Status{isRuning: false}
				timer1 := coinPair.TimerLoop1{Index: i, TaskPro: tasks, Stats: stats}
				tasks.timerAddTask(timer1.Run, i, time.Duration(item.Second)*time.Second)
				go timer1.Run(i)
			} else if item.Name == "lockCoin" {
				stats := &Status{isRuning: false}
				timer2 := lockCoin.TimerLoop2{Index: i, TaskPro: tasks, Stats: stats}
				tasks.timerAddTask(timer2.Run, i, time.Duration(item.Second)*time.Second)
				go timer2.Run(i)
			} else if item.Name == "coinPrice" {
				stats := &Status{isRuning: false}
				timer3 := coinPrice.TimerLoop3{Index: i, TaskPro: tasks, Stats: stats}
				tasks.timerAddTask(timer3.Run, i, time.Duration(item.Second)*time.Second)
				go timer3.Run(i)
			}
		}
		tasks.timer.timeloop.Start() //任务必须先设置 然后在start
	}

	return nil
}

func (tasks *TaskProcess) timerAddTask(f func(index int), index int, interval time.Duration) {
	if tasks.timer != nil {
		tasks.timer.timeloop.Job(f, index, interval)
	}
}
