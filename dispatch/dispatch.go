package dispatch

import (
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"syscall"
	loger "taskhub/common/loger"
	util "taskhub/common/util"
	config "taskhub/config"
	modcom "taskhub/models/common"
	"time"

	"github.com/shopspring/decimal"
)

//价格缓存时间 2分钟  milliseconds
const CACHE_TIMEOUT = 2 * 60
const (
	logModule = "dispatch"
)

var logger = loger.NewLogger(logModule)

type TaskProcess struct {
	homeDir       string
	appConfigFile string
	appConfig     *config.TaskConfig
	sqlxdb        *SqlxDB //没有使用
	gormdb        *GormDB //使用gorm操作数据库
	kafka         *KafkaIns
	redis         *Cache
	timer         *Timer
	loops         *Loops
	mutex         sync.Mutex
	wait          chan bool
	signal        chan os.Signal

	//多个模块公用的缓存数据
	coinSymbolPair *sync.Map //保存ex_pair pair交易对
	lockCoinSymbol *sync.Map //保存目前锁仓的币种
	coinToBtcMap   *sync.Map
}

func NewTaskProcess(homeDir string, configFile string) *TaskProcess {
	tasker := &TaskProcess{
		homeDir:       homeDir,
		appConfigFile: configFile,
	}
	return tasker
}

func (tasks *TaskProcess) init() error {
	err := tasks.initConfig()
	if err != nil {
		return err
	}

	//初始化普通数据库连接池
	err = tasks.OpenSqlxIns()
	if err != nil {
		return err
	}

	//初始化orm数据库连接池
	err = tasks.OpenGormIns()
	if err != nil {
		return err
	}

	err = tasks.initRedis()
	if err != nil {
		return err
	}

	err = tasks.InitKafkaIns()
	if err != nil {
		return err
	}

	err = tasks.InitTimer()
	if err != nil {
		return err
	}

	err = tasks.InitLoops()
	if err != nil {
		return err
	}

	return nil
}

func (tasks *TaskProcess) initConfig() error {
	var err error
	tasks.wait = make(chan bool)
	tasks.signal = make(chan os.Signal, 1)

	signal.Notify(tasks.signal, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT, syscall.SIGUSR1, syscall.SIGUSR2)

	if tasks.homeDir == "" {
		tasks.homeDir, err = os.Getwd()
		if err != nil {
			return fmt.Errorf("Failed to get server's home directory: %s", err)
		}
	}

	var appConfig *config.Config
	if util.Exists(tasks.appConfigFile) {
		appConfig, err = config.InitConfigByType(nil, tasks.appConfigFile, "yaml")
		if err != nil {
			logger.Errorf("InitConfigByType Err %s", err.Error())
			return err
		}
	} else {
		appConfig, err = config.InitConfigByType([]byte(""), "", "yaml")
		if err != nil {
			logger.Errorf("InitConfigByType Err %s", err.Error())
		}
	}

	logger.Debug("start tasks app success.")

	if tasks.appConfig == nil {
		tasks.appConfig = new(config.TaskConfig)
	}

	tasks.appConfig.DataBase, err = appConfig.DBConfig()
	if err != nil {
		logger.Errorf("DataBase Config Err %s", err.Error())
		return err
	} else {
		logger.Debug("tasks.AppConfig.DataBase:")
		logger.Debugf("database type:%s dbsource:%s maxopen:%d maxidle:%d \n",
			tasks.appConfig.DataBase.Type,
			tasks.appConfig.DataBase.DataSource,
			tasks.appConfig.DataBase.MaxOpen,
			tasks.appConfig.DataBase.MaxIdle,
		)
	}

	tasks.appConfig.RedisCache, err = appConfig.CacheConfig()
	if err != nil {
		logger.Errorf("RedisCache Err %s", err.Error())
		return err
	} else {
		logger.Debugln("RedisCache: ", *tasks.appConfig.RedisCache)
	}

	tasks.appConfig.Kafka, err = appConfig.KafkaConfig()
	if err != nil {
		logger.Errorf("KafkaConfig Err %s", err.Error())
		return err
	} else {
		logger.Debugln("KafkaConfig: ", *tasks.appConfig.Kafka)
	}

	tasks.appConfig.Loops, err = appConfig.LoopsSetting()
	if err != nil {
		logger.Errorf("LoopsSetting Err %s", err.Error())
		return err
	} else {
		for index, item := range tasks.appConfig.Loops {
			logger.Debugf("loop:%d Name:%s Desc:%s Cron:%s", index, item.Name, item.Desc, item.Cron)
		}
	}

	tasks.appConfig.Timers, err = appConfig.TimerSetting()
	if err != nil {
		logger.Errorf("LoopsSetting Err %s", err.Error())
		return err
	} else {
		for index, item := range tasks.appConfig.Timers {
			logger.Debugf("timer:%d Name:%s Desc:%s Second:%d", index, item.Name, item.Desc, item.Second)
		}
	}

	tasks.coinSymbolPair = new(sync.Map)
	tasks.lockCoinSymbol = new(sync.Map)
	tasks.coinToBtcMap = new(sync.Map)
	return nil
}

func (tasks *TaskProcess) WaitStopAndDistory() error {
	if tasks == nil {
		return nil
	}
	logger.Debugf("Task Distory Begin.............")
	go func(wait chan bool) {
		for sig := range tasks.signal {
			switch sig {
			case syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT:
				signal.Stop(tasks.signal)
				logger.Debugf("receive signal %v", sig)
				if wait != nil {
					//wait <- true
					close(wait) //notify all and exit
					goto end
				}

				logger.Debugf("receive signal %v", sig)
			default:
				logger.Debugf("receive other signal %v", sig)
			}
		}
	end:
	}(tasks.wait)

	err := checkAndEnableProfiling()
	if err != nil {
		logger.Errorf("checkAndEnableProfiling Err %s", err.Error())
		goto end
	}

	<-tasks.wait

end:
	err = tasks.StopTimer()
	if err != nil {
		logger.Errorf("destory timer Err %s", err.Error())
		return err
	} else {
		logger.Debugf("destory timer success.......")
	}

	err = tasks.StopLoops()
	if err != nil {
		logger.Errorf("destory loop  Err %s", err.Error())
		return err
	} else {
		logger.Debugf("destory loop success.......")
	}

	err = tasks.closeCache()
	if err != nil {
		logger.Errorf("close redis Err %s", err.Error())
		return err
	} else {
		logger.Debugf("close redis success.......")
	}

	err = tasks.DestroyKafkaIns()
	if err != nil {
		logger.Errorf("close kafka Err %s", err.Error())
		return err
	} else {
		logger.Debugf("close kafka success.......")
	}

	err = tasks.CloseSqlxIns()
	if err != nil {
		logger.Errorf("close database common ins Err %s", err.Error())
		return err
	} else {
		logger.Debugf("close database common ins success.......")
	}

	err = tasks.CloseGormIns()
	if err != nil {
		logger.Errorf("close database orm ins Err %s", err.Error())
		return err
	} else {
		logger.Debugf("close database orm ins success.......")
	}

	logger.Debugf("Task Distory End, success............")
	return nil
}

func (tasks *TaskProcess) Start() error {
	var err error
	for {
		err = tasks.init()
		if err != nil {
			break
		}

		err = tasks.TimerRun()
		if err != nil {
			break
		}

		err = tasks.LoopsRun()
		if err != nil {
			break
		}
		break
	}

	if err != nil {
		go func() {
			if tasks.signal != nil {
				tasks.signal <- syscall.SIGINT | syscall.SIGQUIT
			}
		}()

		erro := tasks.WaitStopAndDistory()
		if erro != nil {
			logger.Debugln("call WaitStopAndDistory abort err msg %s", erro.Error())
		}
	} else {
		err = tasks.WaitStopAndDistory()
		if err != nil {
			return err
		}
	}
	return err
}

func (tasks *TaskProcess) LoopsConfig() []config.LoopItem {
	if tasks != nil && tasks.appConfig != nil {
		return tasks.appConfig.Loops
	}
	return nil
}

func (tasks *TaskProcess) TimersConfig() []config.TimerItem {
	if tasks != nil && tasks.appConfig != nil {
		return tasks.appConfig.Timers
	}
	return nil
}

func (tasks *TaskProcess) IsExit() chan bool {
	if tasks != nil {
		return tasks.wait
	}
	return nil
}

//获取expair pair交易对
func (tasks *TaskProcess) GetCoinSymbolPair(coinPair interface{}) bool {
	if tasks != nil {
		_, ok := tasks.coinSymbolPair.Load(coinPair)
		return ok
	}
	return false
}

//设置程序局部pair交易对
func (tasks *TaskProcess) SetCoinSymbolPair(key, value interface{}) *sync.Map {
	if tasks != nil {
		tasks.coinSymbolPair.Store(key, value)
		return tasks.coinSymbolPair
	}
	return nil
}

//判断是否是锁币币种
func (tasks *TaskProcess) IsLockCoinSymbol(coinSymbol string) bool {
	if tasks != nil {
		coinPair := strings.Split(coinSymbol, "_")
		for _, coin := range coinPair {
			_, isExist := tasks.lockCoinSymbol.Load(coin)
			if isExist {
				return true
			}
		}
		return false
	}
	return false
}

func (tasks *TaskProcess) SetLockCoinSymbol(key, value interface{}) *sync.Map {
	if tasks != nil {
		tasks.lockCoinSymbol.Store(key, value)
		return tasks.lockCoinSymbol
	}
	return nil
}

//其它币种对应的Btc价格
func (tasks *TaskProcess) EqualBtcPrice(coinSymbol string) (decimal.Decimal, error) {
	zero := decimal.NewFromFloat(0)
	if tasks != nil {
		if coinSymbol == "BTC" {
			return decimal.NewFromFloat(1), nil
		}

		var keySymbol string
		if coinSymbol == "USDT" || coinSymbol == "DAI" {
			keySymbol = strings.Join([]string{modcom.Prefix_ticker, "BTC_", coinSymbol}, "")
		} else {
			keySymbol = strings.Join([]string{modcom.Prefix_ticker, coinSymbol, "_BTC"}, "")
		}

		pInfo, isExist := tasks.coinToBtcMap.Load(keySymbol)
		if isExist {
			if priceInfo, ok := pInfo.(*modcom.LocPriceInfo); ok {
				if time.Now().Unix()-priceInfo.UpdatedTime < CACHE_TIMEOUT {
					retPrice := decimal.NewFromFloat(priceInfo.Price)
					if coinSymbol == "USDT" || coinSymbol == "DAI" {
						if priceInfo.Price > 0 {
							retPrice = decimal.NewFromFloat(1).Div(retPrice).Truncate(8)
						}
					}
					return retPrice, nil
				}
			}
		}

		keyVal, err := tasks.GetV(keySymbol)
		if err != nil {
			return zero, err
		}

		decoder := json.NewDecoder(strings.NewReader(keyVal))
		decoder.UseNumber()

		var priceInfo interface{}
		if err := decoder.Decode(&priceInfo); err != nil {
			logger.Errorf("Decode %s err %s", keySymbol, err.Error())
			return zero, err
		}

		priceMap := priceInfo.(map[string]interface{})
		price := priceMap["price"]
		var lastPrice float64

		if reflect.TypeOf(price).Name() == "Number" {
			lastPrice, err = price.(json.Number).Float64()
			if err != nil {
				logger.Errorf("TypeOf number %s err %s", keySymbol, err.Error())
				return zero, err
			}
		} else if reflect.TypeOf(price).Name() == "string" {
			lastPrice, err = strconv.ParseFloat(price.(string), 64)
			if err != nil {
				logger.Errorf("TypeOf string %s err %s", keySymbol, err.Error())
				return zero, err
			}
		}

		btcPrice := &modcom.LocPriceInfo{time.Now().Unix(), lastPrice}
		tasks.coinToBtcMap.Store(keySymbol, btcPrice)

		retPrice := decimal.NewFromFloat(lastPrice)
		if coinSymbol == "USDT" || coinSymbol == "DAI" {
			if lastPrice > 0 {
				retPrice = decimal.NewFromFloat(1).Div(retPrice).Truncate(8)
			}
		}

		return retPrice, nil
	}
	return zero, nil
}

//btc对应一个其它币的价格
func (tasks *TaskProcess) BtcEqucalSymbolPrice(coinSymbol string) (decimal.Decimal, error) {
	zero := decimal.NewFromFloat(0)
	if tasks != nil {
		keySymbol := strings.Join([]string{modcom.Prefix_ticker, coinSymbol}, "")
		pInfo, isExist := tasks.coinToBtcMap.Load(keySymbol)
		if isExist {
			//milliseconds
			if priceInfo, ok := pInfo.(*modcom.LocPriceInfo); ok {
				if time.Now().Unix()-priceInfo.UpdatedTime < CACHE_TIMEOUT {
					return decimal.NewFromFloat(priceInfo.Price), nil
				}
			}
		}

		//拉取最新价格
		keyVal, err := tasks.GetV(keySymbol)
		if err != nil {
			return zero, err
		}

		decoder := json.NewDecoder(strings.NewReader(keyVal))
		decoder.UseNumber()

		var priceInfo interface{}
		if err := decoder.Decode(&priceInfo); err != nil {
			logger.Errorf("Decode %s err %s", keySymbol, err.Error())
			return zero, err
		}

		priceMap := priceInfo.(map[string]interface{})
		price := priceMap["price"]
		var lastPrice float64

		if reflect.TypeOf(price).Name() == "Number" {
			lastPrice, err = price.(json.Number).Float64()
			if err != nil {
				logger.Errorf("TypeOf number %s err %s", keySymbol, err.Error())
				return zero, err
			}
		} else if reflect.TypeOf(price).Name() == "string" {
			lastPrice, err = strconv.ParseFloat(price.(string), 64)
			if err != nil {
				logger.Errorf("TypeOf string %s err %s", keySymbol, err.Error())
				return zero, err
			}
		}

		valPrice := &modcom.LocPriceInfo{time.Now().Unix(), lastPrice}
		tasks.coinToBtcMap.Store(keySymbol, valPrice)

		return decimal.NewFromFloat(lastPrice), nil
	}
	return zero, nil
}

func (tasks *TaskProcess) GetBtcPriceMap() *sync.Map {
	if tasks != nil {
		return tasks.coinToBtcMap
	}
	return nil
}
