package dispatch

import (
	util "taskhub/common/util"
	"time"

	sarama "github.com/Shopify/sarama"
)

type KafkaIns struct {
	Producer      sarama.SyncProducer
	Consumer      sarama.Consumer
	IsInitialized bool
}

func (tasks *TaskProcess) InitKafkaIns() error {
	logger.Debug("Initializing Kafka")

	tasks.mutex.Lock()
	defer tasks.mutex.Unlock()

	kafka := tasks.appConfig.Kafka

	prodConfig := sarama.NewConfig()
	prodConfig.Producer.Return.Successes = true
	prodConfig.Producer.RequiredAcks = sarama.WaitForAll
	prodConfig.Producer.Partitioner = sarama.NewRandomPartitioner
	prodConfig.Producer.Compression = sarama.CompressionNone

	prodConfig.Net.MaxOpenRequests = kafka.MaxOpenRequests
	prodConfig.Net.DialTimeout = time.Duration(kafka.DialTimeout) * time.Second
	prodConfig.Net.ReadTimeout = time.Duration(kafka.ReadTimeout) * time.Second
	prodConfig.Net.WriteTimeout = time.Duration(kafka.WriteTimeout) * time.Second
	prodConfig.ClientID = util.RandSeqString(10) + ".producer"

	producer, err := sarama.NewSyncProducer(kafka.Addrs, prodConfig)
	if err != nil {
		logger.Errorf("Kafka producer create err %s", err.Error())
		return err
	}

	consConfig := sarama.NewConfig()
	consConfig.Net.MaxOpenRequests = kafka.MaxOpenRequests
	consConfig.Net.DialTimeout = time.Duration(kafka.DialTimeout) * time.Second
	consConfig.Net.ReadTimeout = time.Duration(kafka.ReadTimeout) * time.Second
	consConfig.Net.WriteTimeout = time.Duration(kafka.WriteTimeout) * time.Second
	consConfig.ClientID = util.RandSeqString(10) + ".consumer"
	consConfig.ChannelBufferSize = 500
	consumer, err := sarama.NewConsumer(kafka.Addrs, consConfig)
	if err != nil {
		logger.Errorf("Kafka consumer create err %s", err.Error())
		return err
	}

	tasks.kafka = &KafkaIns{producer, consumer, true}
	return nil
}

func (tasks *TaskProcess) DestroyKafkaIns() error {
	var retErr error
	if tasks.kafka != nil {
		if tasks.kafka.Producer != nil {
			err := tasks.kafka.Producer.Close()
			if err != nil {
				retErr = err
			}
		}

		if tasks.kafka.Consumer != nil {
			err := tasks.kafka.Consumer.Close()
			if err != nil {
				retErr = err
			}
		}
	}
	return retErr
}

func (tasks *TaskProcess) GetProducer() sarama.SyncProducer {
	if tasks != nil && tasks.kafka.IsInitialized {
		return tasks.kafka.Producer
	}
	return nil
}

func (tasks *TaskProcess) GetConsumer() sarama.Consumer {
	if tasks != nil && tasks.kafka.IsInitialized {
		return tasks.kafka.Consumer
	}
	return nil
}
