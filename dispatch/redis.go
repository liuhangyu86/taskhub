package dispatch

import (
	"errors"
	"time"

	redis "github.com/go-redis/redis"
)

type Cache struct {
	*redis.Client
	IsCacheInitialized bool
}

func (tasks *TaskProcess) initRedis() error {
	logger.Debug("Initializing Redis")
	var err error
	tasks.mutex.Lock()
	defer tasks.mutex.Unlock()

	if tasks.redis != nil && tasks.redis.IsCacheInitialized {
		return nil
	}

	redisCacheSrv := tasks.appConfig.RedisCache
	logger.Debugf("Initializing '%s' cache at %d begin", "redis", redisCacheSrv.DB)

	client := redis.NewClient(&redis.Options{
		Addr:         redisCacheSrv.Addr,
		Password:     redisCacheSrv.Password, // no password set
		DB:           redisCacheSrv.DB,       // use default DB
		PoolSize:     redisCacheSrv.PoolSize,
		MinIdleConns: redisCacheSrv.MinIdleConns,
		ReadTimeout:  time.Duration(redisCacheSrv.ReadTimeout) * time.Second,
		WriteTimeout: time.Duration(redisCacheSrv.WriteTimeout) * time.Second,
	})

	pong, err := client.Ping().Result()
	if err != nil {
		return err
	}

	tasks.redis = &Cache{client, true}
	logger.Infof("Initialized %s database at %d success, get ping ---> %s", "redis", redisCacheSrv.DB, pong)
	return nil
}

func (tasks *TaskProcess) closeCache() error {
	if tasks.redis != nil && tasks.redis.IsCacheInitialized {
		err := tasks.redis.Close()
		if err != nil {
			return err
		}
		tasks.redis = nil
	}
	return nil
}

func (tasks *TaskProcess) SetKV(key string, value string, expiration time.Duration) error {
	if tasks.redis != nil && tasks.redis.IsCacheInitialized {
		err := tasks.redis.Set(key, value, expiration).Err()
		if err != nil {
			return err
		}
		return nil
	}
	return errors.New("redis not initialized")
}

func (tasks *TaskProcess) GetV(key string) (string, error) {
	if tasks.redis != nil && tasks.redis.IsCacheInitialized {
		return tasks.redis.Get(key).Result()
	}
	return "", errors.New("redis not initialized")
}
