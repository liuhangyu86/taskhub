package dispatch

import (
	"errors"
	"regexp"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

var (
	dbURLRegex = regexp.MustCompile("(Datasource:\\s*)?(\\S+):(\\S+)@|(Datasource:.*\\s)?(user=\\S+).*\\s(password=\\S+)|(Datasource:.*\\s)?(password=\\S+).*\\s(user=\\S+)")
)

type GormDB struct {
	db              *gorm.DB
	IsDBInitialized bool
	dbName          string
}

func MaskDBCred(str string) string {
	matches := dbURLRegex.FindStringSubmatch(str)
	// If there is a match, there should be three entries: 1 for
	// the match and 9 for submatches (see dbURLRegex regular expression)
	if len(matches) == 10 {
		matchIdxs := dbURLRegex.FindStringSubmatchIndex(str)
		substr := str[matchIdxs[0]:matchIdxs[1]]
		for idx := 1; idx < len(matches); idx++ {
			if matches[idx] != "" {
				if strings.Index(matches[idx], "user=") == 0 {
					substr = strings.Replace(substr, matches[idx], "user=****", 1)
				} else if strings.Index(matches[idx], "password=") == 0 {
					substr = strings.Replace(substr, matches[idx], "password=****", 1)
				} else {
					substr = strings.Replace(substr, matches[idx], "****", 1)
				}
			}
		}
		str = str[:matchIdxs[0]] + substr + str[matchIdxs[1]:len(str)]
	}
	return str
}

func (tasks *TaskProcess) OpenGormIns() error {
	var err error
	dbase := tasks.appConfig.DataBase

	dbName := getDBName(dbase.DataSource)
	logger.Debugf("Database Name: %s", dbName)

	ds := dbase.DataSource
	ds = MaskDBCred(ds)
	logger.Debugf("Initializing Gorm '%s' database at '%s'", dbase.Type, ds)

	db, err := gorm.Open(dbase.Type, dbase.DataSource)
	if err != nil {
		return err
	}
	tasks.gormdb = &GormDB{db: db, IsDBInitialized: false, dbName: dbName}

	//and init gorm database handle
	db.DB().SetMaxIdleConns(dbase.MaxIdle)
	db.DB().SetMaxOpenConns(dbase.MaxOpen)
	db.DB().SetConnMaxLifetime(time.Duration(0) * time.Second)
	db.SingularTable(true)

	db.LogMode(dbase.IsLoger)
	//db.SetLogger(log.New(os.Stdout, "gorm: ", log.LstdFlags|log.Lshortfile))
	//db.SetLogger(logger)

	err = db.DB().Ping()
	if err != nil {
		return err
	}
	tasks.gormdb.IsDBInitialized = true
	return nil
}

func (tasks *TaskProcess) CloseGormIns() error {
	if tasks.gormdb != nil && tasks.gormdb.IsDBInitialized {
		return tasks.gormdb.db.Close()
	}
	return errors.New("gorm handle not initialized")
}

func (tasks *TaskProcess) GetGormIns() (*gorm.DB, string, error) {
	if tasks.gormdb != nil && tasks.gormdb.IsDBInitialized {
		return tasks.gormdb.db, tasks.gormdb.dbName, nil
	}
	return nil, "", errors.New("gorm handle not initialized")
}
