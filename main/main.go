package main

import (
	"log"
	"path"
	util "taskhub/common/util"
	dispatch "taskhub/dispatch"
)

func main() {
	homeDir, err := util.GetCurrentDirectory()
	if err != nil {
		log.Printf("getCurrentDirectory %s\n", err.Error())
		return
	}

	tasks := dispatch.NewTaskProcess(
		homeDir,
		path.Join(homeDir, "config/task.yaml"),
	)

	err = tasks.Start()
	if err != nil {
		log.Printf("start task app failture, %s\n", err.Error())
		return
	}
}
