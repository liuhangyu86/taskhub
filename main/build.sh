#!/bin/bash
function printHelp() {
  echo "Usage: "
  echo "    -d build debug bin"
  echo "    -r build release bin"
  echo "    -c clean bin"
}

function build_debug() {
    #go build -gcflags '-N -l' -o taskhub main.go
    #多核编译
    go build  -gcflags '-c 4'  -o taskhub main.go
}

function build_release() {
    go build -ldflags '-w -s' -o  taskhub main.go
}

function clean() {
    rm -rf taskhub
}

function run() {
   ./taskhub
}

if [ "$1" == "-d" ]; then
  build_debug
elif [ "$1" == "-r" ]; then ## Clear the network
  build_release
elif [ "$1" == "-c" ]; then ## Clear the network
  clean
else 
  build_debug
  exit 1
fi

