package config

import (
	"bytes"
	"errors"
	"fmt"
	"strings"
	loger "taskhub/common/loger"

	viper "github.com/spf13/viper"
)

const (
	cmdRoot        = "AppTask"
	MAX_IDLE_CONNS = 1000
	MAX_OPEN_CONNS = 100

	REDIS_PWD            = "Biiigle1234"
	REDIS_DBID           = 0
	REDIS_POOL_SIZE      = 10
	REDIS_MIN_IDLE_CONNS = 10
	REDIS_READ_TIMEOUT   = 5
	REDIS_WRITE_TIMEOUT  = 5

	KAFKA_MaxOpen_Requests = 100
	KAFKA_Dial_Timeout     = 10
	KAFKA_Read_Timeout     = 10
	KAFKA_Write_Timeout    = 10

	logModule0 = "all"
	logModule1 = "dispatch"
	logModule2 = "signUpShare"
	logModule3 = "tradeMonitor"
	logModule4 = "job3"
	logModule5 = "job4"
	logModule6 = "job5"

	logModule101 = "coinPair"
	logModule102 = "lockCoin"
	logModule103 = "coinPrice"
	logModule104 = "timer4"
	logModule105 = "timer5"
)

type ServerTLSConfig struct {
	Enabled  bool
	CertFile string
	KeyFile  string
	CaCert   string
}

type DataBaseConfig struct {
	Type       string
	DataSource string
	MaxIdle    int
	MaxOpen    int
	IsLoger    bool
	TLS        ServerTLSConfig
}

type RedisConfig struct {
	Addr         string
	Password     string
	DB           int
	PoolSize     int
	MinIdleConns int
	ReadTimeout  int
	WriteTimeout int
}

type KafkaCondig struct {
	Addrs           []string
	MaxOpenRequests int
	DialTimeout     int
	ReadTimeout     int
	WriteTimeout    int
}

type LoopItem struct {
	Name string
	Desc string
	Cron string
}

type TimerItem struct {
	Name   string
	Desc   string
	Second int64
}

type TaskConfig struct {
	DataBase   *DataBaseConfig
	RedisCache *RedisConfig
	Kafka      *KafkaCondig
	Loops      []LoopItem
	Timers     []TimerItem
}

type Config struct {
	appConfig       *TaskConfig
	appConfigCached bool
	configViper     *viper.Viper
}

type ModLogLevel struct {
	Module string
	Level  string
}

var GlobalLoglevel loger.Level = loger.INFO

func initModelsLevel(logLevel loger.Level) {
	loger.SetLevel(logModule0, logLevel)
	loger.SetLevel(logModule1, logLevel)
	loger.SetLevel(logModule2, logLevel)
	loger.SetLevel(logModule3, logLevel)
	loger.SetLevel(logModule4, logLevel)
	loger.SetLevel(logModule5, logLevel)
	loger.SetLevel(logModule6, logLevel)

	loger.SetLevel(logModule101, logLevel)
	loger.SetLevel(logModule102, logLevel)
	loger.SetLevel(logModule103, logLevel)
	loger.SetLevel(logModule104, logLevel)
	loger.SetLevel(logModule105, logLevel)
}

func setLogLevel(myViper *viper.Viper) loger.Level {
	var modLogLevel []ModLogLevel
	GlobalLoglevel = loger.INFO
	err := myViper.UnmarshalKey("Logging", &modLogLevel)
	if err != nil {
		initModelsLevel(loger.INFO)
		return GlobalLoglevel
	}

	for _, item := range modLogLevel {
		if item.Module == logModule0 {
			mLogLevel, err := loger.LogLevel(item.Level)
			if err != nil {
				mLogLevel = loger.INFO
			}
			GlobalLoglevel = mLogLevel
			initModelsLevel(GlobalLoglevel)
		} else {
			mLogLevel, err := loger.LogLevel(item.Level)
			if err != nil {
				mLogLevel = loger.INFO
			}
			loger.SetLevel(item.Module, mLogLevel)
		}
	}
	return GlobalLoglevel
}

func InitConfigByType(configBytes []byte, configFile string, configType string) (*Config, error) {
	myViper := viper.New()
	myViper.SetEnvPrefix(cmdRoot)
	myViper.AutomaticEnv()
	replacer := strings.NewReplacer(".", "_")
	myViper.SetEnvKeyReplacer(replacer)

	if configType == "" {
		return nil, errors.New("configType is unknow type")
	}

	if len(configBytes) > 0 {
		buf := bytes.NewBuffer(configBytes)
		//log.Printf("buf Len is %d, Cap is %d: %s\n", buf.Len(), buf.Cap(), buf)
		myViper.SetConfigType(configType)
		myViper.ReadConfig(buf)
	} else {
		if configFile != "" {
			myViper.SetConfigFile(configFile)
			err := myViper.ReadInConfig()

			if err == nil {
				//log.Printf("Using config file: %s\n", myViper.ConfigFileUsed())
			} else {
				return nil, errors.New("loading config file failed")
			}
		}
	}

	GlobalLoglevel = setLogLevel(myViper)
	return &Config{configViper: myViper}, nil
}

func (c *Config) cacheConfiguration() error {
	var err error
	c.appConfig = new(TaskConfig)
	c.appConfig.DataBase = new(DataBaseConfig)
	c.appConfig.RedisCache = new(RedisConfig)
	c.appConfig.Kafka = new(KafkaCondig)

	err = c.configViper.UnmarshalKey("Database", c.appConfig.DataBase)
	//log.Printf("DataBase are: %+v\n", c.appConfig.DataBase)
	if err != nil {
		return err
	} else {
		dataSource := c.configViper.GetString("Database_DataSource")
		if dataSource != "" {
			c.appConfig.DataBase.DataSource = dataSource
			//log.Printf("DataBase are: %+v\n", c.appConfig.DataBase)
		}
	}

	err = c.configViper.UnmarshalKey("Redis", c.appConfig.RedisCache)
	//log.Printf("CacheSrv are: %+v\n", c.appConfig.CacheSrv)
	if err != nil {
		return err
	}

	err = c.configViper.UnmarshalKey("Kafka", c.appConfig.Kafka)
	//log.Printf("Kafka are: %+v\n", c.appConfig.Kafka)
	if err != nil {
		return err
	}

	err = c.configViper.UnmarshalKey("Loops", &c.appConfig.Loops)
	//log.Printf("Loops are: %+v\n", c.appConfig.Loops)
	if err != nil {
		return err
	}

	err = c.configViper.UnmarshalKey("Timer", &c.appConfig.Timers)
	//log.Printf("Timer are: %+v\n", c.appConfig.Timers)
	if err != nil {
		return err
	}

	c.appConfigCached = true
	return err
}

func (c *Config) AppConfig() (*TaskConfig, error) {
	if c.appConfigCached {
		return c.appConfig, nil
	}

	if err := c.cacheConfiguration(); err != nil {
		return nil, errors.New("configuration load failed")
	}
	return c.appConfig, nil
}

func (c *Config) DBConfig() (*DataBaseConfig, error) {
	config, err := c.AppConfig()
	if err != nil {
		return nil, err
	}

	if config.DataBase.Type != "mysql" {
		config.DataBase.Type = "mysql"
	}

	if config.DataBase.DataSource == "" {
		config.DataBase.DataSource = "dev:biiigle1234@tcp(localdb.bibox360.com:3306)/bibox_main?charset=utf8&loc=UTC&parseTime=true"
	}

	if config.DataBase.MaxIdle <= 0 {
		config.DataBase.MaxIdle = MAX_IDLE_CONNS
	}

	if config.DataBase.MaxOpen <= 0 {
		config.DataBase.MaxOpen = MAX_OPEN_CONNS
	}

	if config.DataBase.TLS.Enabled == true {
		if config.DataBase.TLS.CaCert == "" ||
			config.DataBase.TLS.KeyFile == "" ||
			config.DataBase.TLS.CertFile == "" {
			return nil, fmt.Errorf("%s", "cacert, keyfile, certfile file is not setting.")
		}
	}

	return config.DataBase, nil
}

func (c *Config) CacheConfig() (*RedisConfig, error) {
	config, err := c.AppConfig()
	if err != nil {
		return nil, err
	}

	if config.RedisCache.Addr == "" {
		config.RedisCache.Addr = "localdb.bibox360.com:6379"
	}

	if config.RedisCache.Password == "" {
		config.RedisCache.Password = REDIS_PWD
	}

	if config.RedisCache.DB <= 0 {
		config.RedisCache.DB = REDIS_DBID
	}

	if config.RedisCache.PoolSize <= 0 {
		config.RedisCache.PoolSize = REDIS_POOL_SIZE
	}

	if config.RedisCache.MinIdleConns <= 0 {
		config.RedisCache.MinIdleConns = REDIS_MIN_IDLE_CONNS
	}

	if config.RedisCache.ReadTimeout <= 0 {
		config.RedisCache.ReadTimeout = REDIS_READ_TIMEOUT
	}

	if config.RedisCache.WriteTimeout <= 0 {
		config.RedisCache.WriteTimeout = REDIS_WRITE_TIMEOUT
	}

	return config.RedisCache, nil
}

func (c *Config) KafkaConfig() (*KafkaCondig, error) {
	config, err := c.AppConfig()
	if err != nil {
		return nil, err
	}

	if len(config.Kafka.Addrs) == 0 {
		config.Kafka.Addrs = append(config.Kafka.Addrs, "localkfk.bibox360.com:9092")
	}

	if config.Kafka.MaxOpenRequests <= 0 {
		config.Kafka.MaxOpenRequests = KAFKA_MaxOpen_Requests
	}

	if config.Kafka.DialTimeout <= 0 {
		config.Kafka.DialTimeout = KAFKA_Dial_Timeout
	}

	if config.Kafka.ReadTimeout <= 0 {
		config.Kafka.ReadTimeout = KAFKA_Read_Timeout
	}

	if config.Kafka.WriteTimeout <= 0 {
		config.Kafka.WriteTimeout = KAFKA_Write_Timeout
	}

	return config.Kafka, nil
}

func (c *Config) LoopsSetting() ([]LoopItem, error) {
	config, err := c.AppConfig()
	if err != nil {
		return nil, err
	}

	if len(config.Loops) == 0 {
		item1 := LoopItem{Name: "signUpShare", Desc: "签到分红，每隔5分钟执行一次", Cron: "0 */5 * * * ?"}
		item2 := LoopItem{Name: "tradeMonitor", Desc: "统计用户每日交易额及佣金,每周执行一次检查", Cron: "@weekly"}
		config.Loops = append(config.Loops, item1, item2)
	} else {
		for i := 0; i < len(config.Loops); i++ {
			if config.Loops[i].Name == "signUpShare" && config.Loops[i].Cron == "" {
				config.Loops[i].Cron = "0 */5 * * * ?" //每个5分钟执行一次
			} else if config.Loops[i].Name == "tradeMonitor" && config.Loops[i].Cron == "" {
				config.Loops[i].Cron = "@weekly"
			}
		}
	}

	return config.Loops, nil
}

func (c *Config) TimerSetting() ([]TimerItem, error) {
	config, err := c.AppConfig()
	if err != nil {
		return nil, err
	}

	if len(config.Timers) == 0 {
		item1 := TimerItem{Name: "coinPair", Desc: "每5分钟本地更新一个expair.pair交易对", Second: 300}
		item2 := TimerItem{Name: "lockCoin", Desc: "每5分钟本地更新锁币币种信息", Second: 300}
		item3 := TimerItem{Name: "coinPrice", Desc: "每30s本地更新其他币种对价格", Second: 30}
		config.Timers = append(config.Timers, item1, item2, item3)
	} else {
		for i := 0; i < len(config.Timers); i++ {
			if config.Timers[i].Second <= 0 {
				config.Timers[i].Second = 1
			}
		}
	}

	return config.Timers, nil
}
