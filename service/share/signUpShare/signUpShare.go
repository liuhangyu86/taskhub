package signupshare

import (
	"encoding/json"
	"fmt"
	"math"
	"strconv"

	loger "taskhub/common/loger"
	modcom "taskhub/models/common"
	modtab "taskhub/models/tables"
	"time"

	taskapi "taskhub/service"

	redis "github.com/go-redis/redis"
	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"
)

const (
	//活动redis key值
	ActivityKey = "gamesSignUpService.stat"
	DAY         = 24 * 60 * 60
)

const (
	logModule = "signUpShare"
)

var logger = loger.NewLogger(logModule)

type SignUpShareJob struct {
	Index   int
	TaskPro taskapi.TaskProcess
	Stats   taskapi.JobStatus
}

//{"id":122,"number":2000,"type":4,"prize_mode":1,"begin_time":"2019-01-04T00:00:00.000Z","end_time":"2019-12-29T08:00:00.000Z","last_show_time":"2018-12-21T07:52:00.000Z","active":1,"createdAt":"2018-12-22T03:54:05.000Z","updatedAt":"2018-12-22T03:54:05.000Z","desc":null}
func (this SignUpShareJob) Run() {
	if this.TaskPro == nil {
		return
	}

	item := this.TaskPro.LoopsConfig()[this.Index]
	//item := this.tp.appConfig.Loops[this.index]
	logger.Debugf("%s", "signUpShare begin runing......")
	logger.Debugf("signUpShare loops:%d Name:%s Desc:%s Cron:%s",
		this.Index,
		item.Name,
		item.Desc,
		item.Cron,
	)

	db, _, err := this.TaskPro.GetGormIns()
	if err != nil || db == nil {
		logger.Debugf("%s", "signUpShare end")
		return
	}
	db.SetLogger(logger)

	yesterday := time.Now().AddDate(0, 0, -1)

	var raiseFlag modtab.GamesRaiseflag
	activity, err := this.TaskPro.GetV(ActivityKey)
	if err != nil || err == redis.Nil {
		err := db.Where("type = ? and active = ?", modcom.SIGN_UP, 1).First(&raiseFlag).Error
		if err != nil {
			if gorm.IsRecordNotFoundError(err) {
				logger.Debugf("signUpShare %s", "query games_raiseflag record not found")
			} else {
				logger.Debugf("signUpShare query games_raiseflag err %s", err.Error())
			}

			logger.Debugf("%s", "signUpShare end")
			return
		}

		rowBytes, err := json.Marshal(raiseFlag)
		if err != nil {
			logger.Debugf("signUpShare Marshal raiseFlag err %s", err.Error())
			logger.Debugf("%s", "signUpShare end")
			return
		} else {
			activity = string(rowBytes)
		}
		this.TaskPro.SetKV(ActivityKey, activity, 10*60*time.Second)
	} else {
		err := json.Unmarshal([]byte(activity), &raiseFlag)
		if err != nil {
			logger.Debugf("signUpShare Unmarshal GamesRaiseFlagRow err %s", err.Error())
			logger.Debugf("%s", "signUpShare end")
			return
		}
	}

	if activity == "" || raiseFlag.BeginTime.Unix() > time.Now().Unix() || raiseFlag.EndTime.Unix() < time.Now().Unix() {
		logger.Debugln("signUpShare Condition not satisfied activity", activity, time.Now().Unix(), raiseFlag.BeginTime.Unix(), raiseFlag.EndTime.Unix())
		logger.Debugf("%s", "signUpShare end")
		return
	}

	SIGN_UP_FEE := raiseFlag.Number
	for {
		var userSignUp modtab.GamesUserSignUp
		err := db.Where("UNIX_TIMESTAMP(date) < ? and is_sign_up = ? and is_draw = ? ", yesterday, 1, 0).Order("date desc").First(&userSignUp).Error
		if err != nil {
			if gorm.IsRecordNotFoundError(err) {
				logger.Debugf("signUpShare %s", "query first games_user_sign_up record not found")
			} else {
				logger.Debugf("signUpShare query first games_user_sign_up err %s", err.Error())
			}
			logger.Debugf("%s", "signUpShare end")
			return
		}

		fmt.Println("userSignUp", userSignUp)

		//查找未发奖日期
		date := userSignUp.Date.Unix()

		// 总参与人数
		var userSignUpAll []modtab.GamesUserSignUp
		err = db.Where("UNIX_TIMESTAMP(date) = ? and is_sign_up = ?", date, 1).Find(&userSignUpAll).Error
		if err != nil {
			if gorm.IsRecordNotFoundError(err) {
				logger.Debugf("signUpShare %s", "query find games_raiseflag record not found")
			} else {
				logger.Debugf("signUpShare query find games_raiseflag err %s", err.Error())
			}
			logger.Debugf("%s", "signUpShare end")
			return
		}

		// 签到人数
		signInUsersTotal := struct {
			Total int64 `gorm:"column:total"  json:"total"`
		}{Total: 0}

		err = db.Raw("select count(1) as total from games_user_sign_up WHERE UNIX_TIMESTAMP(date) = ? and is_sign_in = ?", date+DAY, 1).Scan(&signInUsersTotal).Error
		if err != nil {
			logger.Debugf("signUpShare query count games_user_sign_up err %s", err.Error())
			logger.Debugf("%s", "signUpShare end")
			return
		}

		pool := decimal.NewFromFloat(math.Ceil(float64(len(userSignUpAll)) * 1.8)).Mul(decimal.NewFromFloat(float64(SIGN_UP_FEE)))
		sign_in_count := decimal.NewFromFloat(math.Ceil(float64(signInUsersTotal.Total) * 1.8))
		//pool := int64(math.Ceil(float64(len(userSignUpAll))*1.8)) * int64(SIGN_UP_FEE)
		//sign_in_count := math.Ceil(float64(signInUsersTotal.Total) * 1.8)

		// 瓜分数量
		var share string
		if sign_in_count.GreaterThan(decimal.NewFromFloat(0)) {
			share = pool.Div(sign_in_count).StringFixed(0)
		} else {
			share = "0"
		}

		for _, item := range userSignUpAll {
			if item.IsDraw == 0 {
				// 发奖
				userSignUp := new(modtab.GamesUserSignUp)
				err := db.Where("UNIX_TIMESTAMP(date) < ? and user_id = ? and is_sign_in = ? ", date+DAY, item.UserID, 1).First(userSignUp).Error
				if err != nil {
					if gorm.IsRecordNotFoundError(err) {
						logger.Debugf("signUpShare query %d games_user_sign_up record not found", item.UserID)
						userSignUp = nil
					} else {
						logger.Debugf("signUpShare query user_id games_user_sign_up err %s", err.Error())
						continue
					}
				}

				tx := db.Begin()
				defer func() {
					if r := recover(); r != nil {
						tx.Rollback()
					}
				}()

				if err := tx.Error; err != nil {
					logger.Debugf("signUpShare db transaction err %s", err.Error())
					continue
				}

				//do SysToUser
				if userSignUp != nil {
					shareFloat64, err := strconv.ParseFloat(share, 64)
					if err != nil {
						logger.Debugf("signUpShare string parse float64 err %s", err.Error())
						continue
					}

					err = modcom.SysToUser(db, tx, userSignUp.ID, userSignUp.UserID, "CP", shareFloat64, 101, modcom.USER_BILLS_GAMES_SIGN_UP, "奖励派发") // 签到活动
					if err != nil {
						tx.Rollback()
						continue
					}
				}

				if userSignUp == nil {
					share_amount := "0"
					var userSignUp modtab.GamesUserSignUp
					err := db.Where("id = ? ", item.ID).First(userSignUp).Error
					if err != nil {
						if gorm.IsRecordNotFoundError(err) {
							logger.Debugf("signUpShare query %d games_user_sign_up record not found", item.UserID)
						} else {
							logger.Debugf("signUpShare query user_id games_user_sign_up err %s", err.Error())
						}

						tx.Rollback()
						continue
					}

					err = tx.Model(&userSignUp).Updates(map[string]interface{}{"is_draw": 1, "share_amount": share_amount}).Error
					if err != nil {
						tx.Rollback()
					}

					tx.Commit()
				} else {
					err := tx.Model(&userSignUp).Updates(map[string]interface{}{"is_draw": 1, "share_amount": share}).Error
					if err != nil {
						tx.Rollback()
					}

					tx.Commit()
				}
			}
		}
	}

	logger.Debugf("%s", "signUpShare end")
}
