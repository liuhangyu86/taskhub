package job3

import (
	loger "taskhub/common/loger"
	taskapi "taskhub/service"
)

const (
	logModule = "job3"
)

var logger = loger.NewLogger(logModule)


type JobLoop3 struct {
	Index   int
	TaskPro taskapi.TaskProcess
	Stats   taskapi.JobStatus
}

func (this JobLoop3) Run() {
	item := this.TaskPro.LoopsConfig()[this.Index]
	logger.Debugf("%s", "job3 begin runing......")

	logger.Debugf("job3 loops:%d Name:%s Desc:%s Cron:%s",
		this.Index,
		item.Name,
		item.Desc,
		item.Cron,
	)

	logger.Debugf("%s", "job3 end")
}
