package timer2

import (
	"sync"
	loger "taskhub/common/loger"
	modcom "taskhub/models/common"
	taskapi "taskhub/service"
	"time"

	"github.com/jinzhu/gorm"
)

const (
	logModule = "lockCoin"
)

var logger = loger.NewLogger(logModule)

type TradeCoinStatus struct {
	CoinSymbol *string    `gorm:"column:coin_symbol" sql:"default null" json:"coin_symbol"`
	SharedDate *time.Time `gorm:"column:shared_date" sql:"default null" json:"shared_date"`
	Find       bool       `gorm:"column:find" sql:"default false" json:"find"`
}

type CoinTypePart struct {
	ID     int64  `gorm:"column:id; AUTO_INCREMENT; primary_key" sql:"not null; type:bigint unsigned" json:"id"`
	Symbol string `gorm:"column:symbol" sql:"not null; type:varchar(16)" json:"symbol"`
}

type TimerLoop2 struct {
	Index   int
	TaskPro taskapi.TaskProcess
	Stats   taskapi.JobStatus
}

func (this TimerLoop2) Run(index int) {
	//协程恢复
	defer func() {
		if r := recover(); r != nil {
			logger.Debugf("%s", "lockCoin recover now")
		}
	}()

	if this.Stats.IsRuning() {
		logger.Debugf("%s", "lockCoin is now runing, this timer plan exit")
		return
	}

	item := this.TaskPro.TimersConfig()[this.Index]
	logger.Debugf("%s", "lockCoin begin runing......")

	logger.Debugf("timer:%d Name:%s Desc:%s Second:%d",
		this.Index,
		item.Name,
		item.Desc,
		item.Second,
	)

	update := func(db *gorm.DB) (*sync.Map, error) {
		var coinStatus []TradeCoinStatus
		err := db.Raw("select coin_symbol, shared_date from token_lock_proj WHERE status = ? and UNIX_TIMESTAMP(list_date) <= ?", modcom.CoinTRADE, time.Now().Unix()).Scan(&coinStatus).Error
		if err != nil {
			return nil, err
		}

		for index, item := range coinStatus {
			if item.SharedDate != nil {
				// if time.Now().Before(*item.SharedDate) || time.Now().Equal(*item.SharedDate) {
				// 	coinStatus[index].Find = true
				// }
				//test
				if time.Now().After(*item.SharedDate) || time.Now().Equal(*item.SharedDate) {
					coinStatus[index].Find = true
				}
			}
		}

		if len(coinStatus) > 0 {
			var coinSymbol []string
			for _, item := range coinStatus {
				if item.Find {
					coinSymbol = append(coinSymbol, *item.CoinSymbol)
				}
			}

			var coinType []CoinTypePart
			err = db.Raw("select id, symbol from coin_type WHERE symbol in (?)", coinSymbol).Scan(&coinType).Error
			if err != nil {
				return nil, err
			}

			var lockCoinMap *sync.Map
			for _, it := range coinType {
				lockCoinMap = this.TaskPro.SetLockCoinSymbol(it.Symbol, it.ID)
			}
			return lockCoinMap, nil
		}
		return nil, nil
	}

	var lockCoinMap *sync.Map
	db, _, err := this.TaskPro.GetGormIns()
	if db == nil {
		logger.Errorf("%s", "gorm handle not initialized")
		goto end
	}
	//db.SetLogger(logger)

	lockCoinMap, err = update(db)
	if err != nil {
		logger.Errorf("update lock coin symbol map err:%s", err.Error())
	}

	lockCoinMap.Range(func(k, v interface{}) bool {
		logger.Debugln(k, v)
		return true
	})

end:
	this.Stats.SetNoRuning()
	logger.Debugf("%s", "lockCoin end")
}
