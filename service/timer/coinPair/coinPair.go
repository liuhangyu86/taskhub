package coinPair

import (
	"sync"
	loger "taskhub/common/loger"
	modtab "taskhub/models/tables"
	taskapi "taskhub/service"

	gorm "github.com/jinzhu/gorm"
)

const (
	logModule = "coinPair"
)

var logger = loger.NewLogger(logModule)

type TimerLoop1 struct {
	Index   int
	TaskPro taskapi.TaskProcess
	Stats   taskapi.JobStatus
}

func (this TimerLoop1) Run(index int) {
	//协程恢复
	defer func() {
		if r := recover(); r != nil {
			logger.Errorf("%s", "coinPair recover now")
		}
	}()

	if this.Stats.IsRuning() {
		logger.Debugf("%s", "coinPair is now runing, this timer plan exit")
		return
	}

	item := this.TaskPro.TimersConfig()[this.Index]
	logger.Debugf("%s", "coinPair begin runing......")

	logger.Debugf("timer:%d Name:%s Desc:%s Second:%d",
		this.Index,
		item.Name,
		item.Desc,
		item.Second,
	)

	queryExPairTab := func(db *gorm.DB) ([]modtab.ExPair, error) {
		var exPair []modtab.ExPair
		err := db.Where("pair_type = ?", 0).Find(&exPair).Error
		if err != nil {
			if gorm.IsRecordNotFoundError(err) {
				logger.Debugf("coinPair %s", "query find exPair record not found")
			} else {
				logger.Debugf("coinPair query find exPair err %s", err.Error())
			}
			return nil, err
		}
		return exPair, nil
	}

	update := func(db *gorm.DB) (*sync.Map, error) {
		expair, err := queryExPairTab(db)
		if err != nil {
			return nil, err
		}

		var syncMap *sync.Map
		for _, item := range expair {
			syncMap = this.TaskPro.SetCoinSymbolPair(item.Pair, 1)
		}
		return syncMap, nil
	}

	//var pairMap *sync.Map
	db, _, err := this.TaskPro.GetGormIns()
	if db == nil {
		logger.Errorf("%s", "gorm handle not initialized")
		goto end
	}
	//db.SetLogger(logger)

	_, err = update(db)
	if err != nil {
		logger.Errorf("update coin symbol pair err:%s", err.Error())
	}

	// pairMap.Range(func(k, v interface{}) bool {
	// 	logger.Debugln(k, v)
	// 	return true
	// })

end:
	this.Stats.SetNoRuning()
	logger.Debugf("%s", "coinPair end")
}
