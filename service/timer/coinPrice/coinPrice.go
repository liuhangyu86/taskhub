package timer3

import (
	"encoding/json"
	"reflect"
	"strconv"
	"strings"
	loger "taskhub/common/loger"
	modcom "taskhub/models/common"
	taskapi "taskhub/service"
	"time"
)

const CACHE_TIMEOUT = 2 * 60
const (
	logModule = "coinPrice"
)

var logger = loger.NewLogger(logModule)

type TimerLoop3 struct {
	Index   int
	TaskPro taskapi.TaskProcess
	Stats   taskapi.JobStatus
}

//定时本地更新
func (this TimerLoop3) Run(index int) {
	//协程恢复
	defer func() {
		if r := recover(); r != nil {
			logger.Debugf("%s", "coinPrice recover now")
		}
	}()

	if this.Stats.IsRuning() {
		logger.Debugf("%s", "coinPrice is now runing, this timer plan exit")
		return
	}

	item := this.TaskPro.TimersConfig()[this.Index]
	logger.Debugf("%s", "coinPrice begin runing......")

	logger.Debugf("timer:%d Name:%s Desc:%s Second:%d",
		this.Index,
		item.Name,
		item.Desc,
		item.Second,
	)

	toBtcMap := this.TaskPro.GetBtcPriceMap()

	updatePrice := func(coinPairSymbol, locPrice interface{}) bool {
		if priceInfo, ok := locPrice.(*modcom.LocPriceInfo); ok {
			if time.Now().Unix()-priceInfo.UpdatedTime >= CACHE_TIMEOUT {
				//拉取最新交易对价格
				// keyVal, err := this.TaskPro.GetV(coinPairSymbol.(string))
				// if err != nil {
				// 	return true //继续循环,不执行下面的
				// }

				// var priceInfo modcom.PriceInfo
				// err = json.Unmarshal([]byte(keyVal), &priceInfo)
				// if err != nil {
				// 	return true //继续循环,不执行下面的
				// }

				// price := priceInfo.Price
				// var lastPrice float64
				// switch price.(type) {
				// case string:
				// 	lastPrice, err = strconv.ParseFloat(price.(string), 64)
				// 	if err != nil {
				// 		return true //继续循环,执行下面的
				// 	}
				// case float64:
				// 	lastPrice = price.(float64)
				// }

				// //toBtcMap 保存的原始币种key-v数据，不对应币转btc转化，在接口做处理
				// valPrice := &modcom.LocPriceInfo{time.Now().Unix(), lastPrice}
				// toBtcMap.Store(coinPairSymbol, valPrice)

				//解决数据精度问题以及price数据string和float兼容
				//拉取最新交易对价格
				keyVal, err := this.TaskPro.GetV(coinPairSymbol.(string))
				if err != nil {
					logger.Errorf("GetV %s err %s", coinPairSymbol.(string), err.Error())
					return true //继续循环,不执行下面的
				}
				decoder := json.NewDecoder(strings.NewReader(keyVal))
				decoder.UseNumber()

				var priceInfo interface{}
				if err := decoder.Decode(&priceInfo); err != nil {
					logger.Errorf("Decode %s err %s", coinPairSymbol.(string), err.Error())
					return true
				}

				priceMap := priceInfo.(map[string]interface{})
				price := priceMap["price"]
				var lastPrice float64

				if reflect.TypeOf(price).Name() == "Number" {
					lastPrice, err = price.(json.Number).Float64()
					if err != nil {
						logger.Errorf("TypeOf number %s err %s", coinPairSymbol.(string), err.Error())
						return true
					}
				} else if reflect.TypeOf(price).Name() == "string" {
					lastPrice, err = strconv.ParseFloat(price.(string), 64)
					if err != nil {
						logger.Errorf("TypeOf string %s err %s", coinPairSymbol.(string), err.Error())
						return true
					}
				}

				//toBtcMap 保存的原始币种key-v数据，不对应币转btc转化，在接口做处理
				valPrice := &modcom.LocPriceInfo{time.Now().Unix(), lastPrice}
				toBtcMap.Store(coinPairSymbol, valPrice)
			}
		}
		//return false //当返回false时，遍历立刻结束
		return true //继续循环
	}

	toBtcMap.Range(updatePrice)

	this.Stats.SetNoRuning()
	logger.Debugf("%s", "coinPrice end")
}
