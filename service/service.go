package service

import (
	"sync"
	"taskhub/config"
	"time"

	"github.com/Shopify/sarama"
	"github.com/jinzhu/gorm"
	"github.com/jmoiron/sqlx"
	"github.com/shopspring/decimal"
)

type TaskProcess interface {
	//redis设置kv或者获取k值
	SetKV(key string, value string, expiration time.Duration) error
	GetV(key string) (string, error)

	//sqlx和gorm两种操作数据库的对象
	GetSqlxIns() (*sqlx.DB, string, error)
	GetGormIns() (*gorm.DB, string, error)

	//获取loop配置信息
	LoopsConfig() []config.LoopItem
	//获取timer配置信息
	TimersConfig() []config.TimerItem

	//主进程退出信息
	IsExit() chan bool

	//kafka获取生产者和消费者对象
	GetProducer() sarama.SyncProducer
	GetConsumer() sarama.Consumer

	//缓存交易对，设置以及获取expair pair交易对数据
	GetCoinSymbolPair(coinPair interface{}) bool
	SetCoinSymbolPair(key, value interface{}) *sync.Map

	//判断是否是锁币币种
	IsLockCoinSymbol(coinSymbol string) bool
	SetLockCoinSymbol(key, value interface{}) *sync.Map

	//其它币种对应的Btc价格
	EqualBtcPrice(coinSymbol string) (decimal.Decimal, error)
	//btc对应一个其它币的价格
	BtcEqucalSymbolPrice(coinSymbol string) (decimal.Decimal, error)
	GetBtcPriceMap() *sync.Map
}

//标记jobloop 状态，运行和非运行状态
type JobStatus interface {
	IsRuning() bool
	SetNoRuning() bool
}
