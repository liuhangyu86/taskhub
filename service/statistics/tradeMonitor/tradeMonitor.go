package trademonitor

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"sync"
	loger "taskhub/common/loger"
	util "taskhub/common/util"
	modtab "taskhub/models/tables"
	taskapi "taskhub/service"
	"time"

	sarama "github.com/Shopify/sarama"
	redis "github.com/go-redis/redis"
	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"
)

const (
	Topic_Order_Matched_4User = "testGo-x1" //test //"topic_order_match_4user" //用户订单成交数据持久化完成后通知 {成交详单}
	trade_watch_key           = "trade_watch_key"
	TimeFormat                = "2006-01-02 15:04:05"
	STOP_MINING_TIME          = "2019-01-03 00:00:00"
	STOP_MINING_TIME_SECONDS  = 1546473600 //format STOP_MINING_TIME to unix second
	TIME_OUT_UPDATE           = 30
	RECORD_SUM                = 10 // test 500
)

type ORDER_TYPE_FREEZE int

const (
	FREEZE_MARKET ORDER_TYPE_FREEZE = 1 //市价单
	FREEZE_LIMIT  ORDER_TYPE_FREEZE = 2 //限价单
)

type ORDER_SIDE_FREEZE int

const (
	FREEZE_BID ORDER_SIDE_FREEZE = 1 //bid
	FREEZE_ASK ORDER_SIDE_FREEZE = 2 //ask
)

type PAY_BIX_FREEZE int

const (
	PAY_BIX_FREEZE_NO     PAY_BIX_FREEZE = 0 //不使用bix抵扣手续费
	PAY_BIX_FREEZE_YES    PAY_BIX_FREEZE = 1 //使用bix抵扣手续费
	PAY_BIX_FREEZE_NO_CP  PAY_BIX_FREEZE = 2 //不使用cp抵扣手续费
	PAY_BIX_FREEZE_YES_CP PAY_BIX_FREEZE = 3 //使用cp抵扣手续费
)

const OneHour = 60 * 60

const (
	logModule = "tradeMonitor"
)

var logger = loger.NewLogger(logModule)

type TradeMonitorJob struct {
	Index   int
	TaskPro taskapi.TaskProcess
	Stats   taskapi.JobStatus
}

type GoState int

const (
	Init GoState = iota
	Runing
	Stopped
)

//性能优化
type TradeSet struct {
	UserID             int64           `json:"user_id"`
	TradeDate          int64           `json:"trade_date"`
	EqualBtc           decimal.Decimal `json:"equal_btc"`
	EqualUsdt          decimal.Decimal `json:"equal_usdt"`
	EqualBtcFree       decimal.Decimal `json:"equal_btc_free"`
	EqualUsdtFree      decimal.Decimal `json:"equal_usdt_free"`
	EqualBtcMine       decimal.Decimal `json:"equal_btc_mine"`
	EqualUsdtMine      decimal.Decimal `json:"equal_usdt_mine"`
	EqualBtcMineFree   decimal.Decimal `json:"equal_btc_mine_free"`
	EqualUsdtMineFree  decimal.Decimal `json:"equal_usdt_mine_free"`
	EqualBtcShare      decimal.Decimal `json:"equal_btc_share"`
	EqualUsdtShare     decimal.Decimal `json:"equal_usdt_share"`
	EqualBtcShareFree  decimal.Decimal `json:"equal_btc_share_free"`
	EqualUsdtShareFree decimal.Decimal `json:"equal_usdt_share_free"`
}

type TradeFeeSet struct {
	UserID        int64
	CoinID        int
	TradeDate     int64
	FeeCoinSymbol string
	Fee           decimal.Decimal
	FeeEqualBTC   decimal.Decimal

	EqualUsdtAmount decimal.Decimal
	EqualUsdtReal   decimal.Decimal

	MakerFee             decimal.Decimal
	MakerFeeEqualBTC     decimal.Decimal
	MakerEqualUsdtAmount decimal.Decimal

	CoinSymbol     string
	CurrencySymbol string
	OrderSide      int
	DealMoney      decimal.Decimal
	DealAmount     decimal.Decimal
}

type GoDealStatus struct {
	mqOffset    int64
	isRuning    GoState
	redisKey    string
	kaPartition int32
	pc          sarama.PartitionConsumer
	preOffset   int64 //预处理的偏移量

	//tradeDetailMap  map[int64]map[bool]map[int64]*TradeSet
	tradeDetailMap  map[int64]map[int64]*TradeSet //map1->userid  map2->trade_date
	tradeDetailTime int64                         //入库时间

	feeDetailLockMap  map[int64]map[int]map[int64]*TradeFeeSet //map--userid map2--coin_id  map3--trade_date
	feeDetailLockTime int64                                    //入库时间

	feeDetailAllMap  map[int64]map[int]map[int64]*TradeFeeSet //map--userid map2--coin_id  map3--trade_date
	feeDetailAllTime int64                                    //入库时间
}

type TxDetail struct {
	ID           int64                `json:"id"`
	PendingOrder modtab.ExTradeDetail `json:"pendingOrder"`
}

var gCPCoinID int64

func (this TradeMonitorJob) Run() {
	if this.TaskPro == nil {
		return
	}

	//协程恢复
	defer func() {
		if r := recover(); r != nil {
			logger.Debugf("%s", "tradeMonitor recover now")
		}
	}()

	if this.Stats.IsRuning() {
		logger.Debugf("%s", "tradeMonitor is now runing, this job plan exit")
		return
	}

	//退出
	isExit := make(chan bool)
	defer func() {
		close(isExit)
		logger.Debugf("tradeMonitor end, %d", util.GoID())
	}()

	item := this.TaskPro.LoopsConfig()[this.Index]
	logger.Debugf("tradeMonitor begin runing...... %d", util.GoID())
	logger.Debugf("tradeMonitor loops:%d Name:%s Desc:%s Cron:%s",
		this.Index,
		item.Name,
		item.Desc,
		item.Cron,
	)

	db, _, err := this.TaskPro.GetGormIns()
	if db == nil {
		return
	}
	//debug sql
	//db.SetLogger(logger)

	coinId, err := GetCoinId(db, "CP")
	if err != nil {
		this.Stats.SetNoRuning()
		logger.Debugf("get CP coin id err %s", err.Error())
		return
	} else {
		gCPCoinID = coinId
	}

	err = tradingListener(this.TaskPro, isExit)
	if err != nil {
		this.Stats.SetNoRuning()
		logger.Debugf("tradeMonitor tradingListener err %s", err.Error())
		return
	}
}

func GetCoinId(db *gorm.DB, cionSymbl string) (int64, error) {
	coinID := struct {
		ID int64 `gorm:"column:id; AUTO_INCREMENT; primary_key" sql:"not null; type:bigint unsigned" json:"id"`
	}{ID: 0}

	err := db.Raw("select id from coin_type WHERE symbol = ?", cionSymbl).Scan(&coinID).Error
	if err != nil {
		return -1, err
	}
	return coinID.ID, nil
}

func getMqOffset(tp taskapi.TaskProcess, redisKey string) (int64, error) {
	activity, err := tp.GetV(redisKey)
	if err != nil {
		if err == redis.Nil {
			return sarama.OffsetNewest, nil
		} else {
			return 0, err
		}
	}
	mqOffset, err := strconv.ParseInt(activity, 10, 64)
	if err != nil {
		return 0, err
	}

	_ = mqOffset
	//return mqOffset, nil

	//test
	return sarama.OffsetNewest, nil
}

func tradingListener(tp taskapi.TaskProcess, isExit chan bool) error {
	if tp == nil {
		return errors.New("ins is nil")
	}

	partitionList, err := tp.GetConsumer().Partitions(Topic_Order_Matched_4User)
	if err != nil || len(partitionList) == 0 {
		logger.Errorf("kafka Consumer get topic partition  err %s", err.Error())
		return err
	}

	if len(partitionList) != 1 {
		return errors.New("tradeMonitor only one data slice is supported")
	}

	goDealStatus := make([]GoDealStatus, len(partitionList))
	for index, partition := range partitionList {
		goDealStatus[index].pc = nil
		goDealStatus[index].isRuning = Init
		if index == 0 {
			goDealStatus[index].redisKey = trade_watch_key
		} else {
			goDealStatus[index].redisKey = strings.Join([]string{trade_watch_key, strconv.FormatInt(int64(index), 10)}, "")
		}
		goDealStatus[index].kaPartition = partition

		offset, err := getMqOffset(tp, goDealStatus[index].redisKey)
		if err != nil {
			return err
		}
		goDealStatus[index].mqOffset = offset
		goDealStatus[index].preOffset = offset //初始处理的偏移和redis一致

		//tradeDetailMap 初始化 成为协程局部数据
		//goDealStatus[index].tradeDetailMap = make(map[int64]map[bool]map[int64]*TradeSet)
		goDealStatus[index].tradeDetailMap = make(map[int64]map[int64]*TradeSet)
		goDealStatus[index].tradeDetailTime = time.Now().Unix()

		goDealStatus[index].feeDetailLockMap = make(map[int64]map[int]map[int64]*TradeFeeSet)
		goDealStatus[index].feeDetailLockTime = time.Now().Unix()

		goDealStatus[index].feeDetailAllMap = make(map[int64]map[int]map[int64]*TradeFeeSet)
		goDealStatus[index].feeDetailAllTime = time.Now().Unix()
	}

	var mErr error
	for index, partition := range partitionList {
		if goDealStatus[index].kaPartition != partition {
			mErr = errors.New("index does not match")
		}

		goDealStatus[index].pc, err = tp.GetConsumer().ConsumePartition(Topic_Order_Matched_4User, int32(partition), goDealStatus[index].mqOffset)
		if err != nil {
			logger.Debugf("kafka get partition or mq offset err %s", err.Error())
			mErr = err
		} else {
			logger.Debugf("tradeMonitor index %d begin work kafka offset %d", index, goDealStatus[index].mqOffset)
		}
	}

	if mErr != nil {
		for _, item := range goDealStatus {
			if item.pc != nil {
				item.pc.Close()
			}
		}
		return mErr
	} else {
		var wg sync.WaitGroup
		wg.Add(len(goDealStatus) + 1) //加一个协程检测

		//跟据kafka分片数据，提高吞吐量，需要在协程中处理并发量
		for i := 0; i < len(goDealStatus); i++ {
			go func(index int) {
				dealMqMessage(tp, &wg, goDealStatus, index, isExit) //启动消息处理
			}(i)
		}

		//Health check
		go health(tp, &wg, goDealStatus, isExit)

		wg.Wait()
	}
	return nil
}

func health(tp taskapi.TaskProcess, wg *sync.WaitGroup, goStatus []GoDealStatus, isExit chan bool) {
	defer wg.Done()
	for {
		select {
		case <-time.After(5 * time.Second):
			for i, item := range goStatus { //循环判断检测
				if item.isRuning == Stopped { //停止则启动
					wg.Add(1)
					go func(index int) {
						//缓存的偏移和redis偏移不一致，说明abort过,出现异常  需要调整mq偏移
						if goStatus[index].preOffset != goStatus[index].mqOffset {
							offset, err := getMqOffset(tp, goStatus[index].redisKey)
							if err != nil {
								logger.Debugf("tradeMonitor getMqOffset err %s", err.Error())
								return
							}

							goStatus[index].pc, err = tp.GetConsumer().ConsumePartition(Topic_Order_Matched_4User, int32(goStatus[index].kaPartition), offset)
							if err != nil {
								logger.Debugf("tradeMonitor health ConsumePartition err %s", err.Error())
								return
							}

							goStatus[index].mqOffset = offset
							goStatus[index].preOffset = offset
						}

						dealMqMessage(tp, wg, goStatus, index, isExit)
					}(i)
				}
			}
		case <-tp.IsExit(): //系统退出
			return
		case <-isExit:
			return
		}
	}
}

func dealMqMessage(tp taskapi.TaskProcess, wg *sync.WaitGroup, goDeal []GoDealStatus, index int, isExit chan bool) {
	defer func() {
		if r := recover(); r != nil {
			logger.Debugf("%s %d", "dealMqMessage recover now", index)
		}
	}()

	defer wg.Done()

	defer func() {
		goDeal[index].isRuning = Stopped
		goDeal[index].pc.Close()
	}()

	goDeal[index].isRuning = Runing

	logger.Debugf("dealMqMessage %d begin work offset %d ", index, goDeal[index].mqOffset)
	for {
		var message []*sarama.ConsumerMessage
		for {
			select {
			case msg := <-goDeal[index].pc.Messages():
				message = append(message, msg)
				if len(message) >= RECORD_SUM {
					goto end
				}
			case <-time.After(20 * time.Second):
				goto end
			case <-tp.IsExit(): //系统退出
				goDeal[index].pc.AsyncClose()
				return
			case <-isExit: //携程退出
				goDeal[index].pc.AsyncClose()
				return
			}
		}

	end:
		if len(message) == 0 {
			continue
		} else {
			//设置预处理的偏离量
			goDeal[index].preOffset = message[len(message)-1].Offset + 1
		}

		var isBatchUpdate bool = false   //表示是否有批量更新
		var isFeeLockUpdate bool = false //表示是否锁仓上币交易
		var tx *sql.Tx = nil
		txCount := 0

		for _, msg := range message {
			var trade TxDetail
			err := json.Unmarshal(msg.Value, &trade)
			if err != nil {
				logger.Errorf("dealMqMessage Unmarshal ExTradeDetail err %s\n", err.Error())
				return
			}

			tradeDetail := &trade.PendingOrder
			if trade.ID > 0 && tradeDetail.DealMoney > 0 {
				txCount++
				logger.Debugf("Go:%d Partition:%d, Offset:%d, Len(msg):%d, count:%d\n", index, msg.Partition, msg.Offset, len(message), txCount)

				tokenLock := tp.IsLockCoinSymbol(tradeDetail.Pair)
				//test
				logger.Debugf("%+v %v\n", trade, tokenLock)

				//交易量统计
				_, err = updateDailyTradeing(tp, tradeDetail, tokenLock, goDeal, index)
				if err != nil {
					logger.Errorf("dealMqMessage updateDailyTradeing err %s\n", err.Error())
					return
				}

				//无论是否最后一个交易，满足条件则更新数据库
				isBatchUpdate, tx, err = writeDailyTradeToDB(tp, goDeal, index)
				if err != nil {
					logger.Errorf("dealMqMessage writeDailyTradeToDB err %s\n", err.Error())
					if tx != nil {
						tx.Rollback()
					}
					return
				}

				//手续费统计
				updateDailyFee(tp, tradeDetail, tokenLock, goDeal, index)

				//isUpdateDB 联动更新数据库，保持一直,佣金   每笔交易必然有手续费
				//锁仓上币交易，交易量在kafka直接传递过来的，统计积攒数量直接更新数据库
				isFeeLockUpdate, err = writeFeeDetailLockToDB(tp, tx, goDeal, index, isBatchUpdate)
				if err != nil {
					logger.Errorf("dealMqMessage writeFeeDetailLockToDB err %s\n", err.Error())
					if tx != nil {
						tx.Rollback()
					}
					return
				}

				isBatchUpdate, err = writeFeeDetailAllToDB(tp, tx, goDeal, index, isBatchUpdate)
				if err != nil {
					logger.Errorf("dealMqMessage writeFeeDetailAllToDB err %s\n", err.Error())
					if tx != nil {
						tx.Rollback()
					}
					return
				}

				if isBatchUpdate && tx != nil {
					err = tx.Commit()
					if err != nil {
						logger.Errorf("dealMqMessage Commit %s", err.Error())
					}
				}
			}
		}

		//isBatchUpdate 有更新数据库，则msg完全写入db，则更新redis
		if isBatchUpdate || isFeeLockUpdate {
			//异常循环都没有失败，则处理了len(message)消息，则写入新的偏移量到redis中
			err := tp.SetKV(goDeal[index].redisKey, strconv.FormatInt(goDeal[index].preOffset, 10), 0)
			if err != nil {
				logger.Errorf("dealMqMessage SetKV new redisKey offset err %s\n", err.Error())
				return
			}
			goDeal[index].mqOffset = goDeal[index].preOffset //最终一致
		}
	} //end for
}

func NewTradeSet(pTradeSet *TradeSet, isFree bool, isMine bool, isShare bool, equalBTC decimal.Decimal) *TradeSet {
	if pTradeSet == nil {
		pTradeSet = &TradeSet{}
		if isFree { //免手续费
			//oHourTrade.IsFree = true
			pTradeSet.EqualBtc = decimal.NewFromFloat32(0)
			pTradeSet.EqualUsdt = decimal.NewFromFloat32(0)

			pTradeSet.EqualBtcFree = equalBTC
			pTradeSet.EqualUsdtFree = equalBTC

			pTradeSet.EqualBtcMine = decimal.NewFromFloat32(0)
			pTradeSet.EqualUsdtMine = decimal.NewFromFloat32(0)

			if isMine {
				pTradeSet.EqualBtcMineFree = equalBTC
				pTradeSet.EqualUsdtMineFree = equalBTC
			} else {
				pTradeSet.EqualBtcMineFree = decimal.NewFromFloat32(0)
				pTradeSet.EqualUsdtMineFree = decimal.NewFromFloat32(0)
			}

			pTradeSet.EqualBtcShare = decimal.NewFromFloat32(0)
			pTradeSet.EqualUsdtShare = decimal.NewFromFloat32(0)

			if isShare {
				pTradeSet.EqualBtcShareFree = equalBTC
				pTradeSet.EqualUsdtShareFree = equalBTC
			} else {
				pTradeSet.EqualBtcShareFree = decimal.NewFromFloat32(0)
				pTradeSet.EqualUsdtShareFree = decimal.NewFromFloat32(0)
			}
		} else {
			//oHourTrade.IsFree = false
			pTradeSet.EqualBtc = equalBTC
			pTradeSet.EqualUsdt = equalBTC

			pTradeSet.EqualBtcFree = decimal.NewFromFloat32(0)
			pTradeSet.EqualUsdtFree = decimal.NewFromFloat32(0)

			if isMine {
				pTradeSet.EqualBtcMine = equalBTC
				pTradeSet.EqualUsdtMine = equalBTC
			} else {
				pTradeSet.EqualBtcMine = decimal.NewFromFloat32(0)
				pTradeSet.EqualUsdtMine = decimal.NewFromFloat32(0)
			}

			pTradeSet.EqualBtcMineFree = decimal.NewFromFloat32(0)
			pTradeSet.EqualUsdtMineFree = decimal.NewFromFloat32(0)

			if isShare {
				pTradeSet.EqualBtcShare = equalBTC
				pTradeSet.EqualUsdtShare = equalBTC
			} else {
				pTradeSet.EqualBtcShare = decimal.NewFromFloat32(0)
				pTradeSet.EqualUsdtShare = decimal.NewFromFloat32(0)
			}
		}

		return pTradeSet
	} else {
		if isFree { //免手续费
			pTradeSet.EqualBtcFree = pTradeSet.EqualBtcFree.Add(equalBTC)
			pTradeSet.EqualUsdtFree = pTradeSet.EqualUsdtFree.Add(equalBTC)

			if isMine {
				pTradeSet.EqualBtcMineFree = pTradeSet.EqualBtcMineFree.Add(equalBTC)
				pTradeSet.EqualUsdtMineFree = pTradeSet.EqualBtcMineFree.Add(equalBTC)
			}

			if isShare {
				pTradeSet.EqualBtcShareFree = pTradeSet.EqualBtcShareFree.Add(equalBTC)
				pTradeSet.EqualUsdtShareFree = pTradeSet.EqualUsdtShareFree.Add(equalBTC)
			}
		} else {
			pTradeSet.EqualBtc = pTradeSet.EqualBtc.Add(equalBTC)
			pTradeSet.EqualUsdt = pTradeSet.EqualUsdt.Add(equalBTC)

			if isMine {
				pTradeSet.EqualBtcMine = pTradeSet.EqualBtcMine.Add(equalBTC)
				pTradeSet.EqualUsdtMine = pTradeSet.EqualUsdtMine.Add(equalBTC)
			}

			if isShare {
				pTradeSet.EqualBtcShare = pTradeSet.EqualBtcShare.Add(equalBTC)
				pTradeSet.EqualUsdtShare = pTradeSet.EqualUsdtShare.Add(equalBTC)
			}
		}
		return pTradeSet
	}
	return nil
}

//更新内存交易量
func updateDailyTradeing(tp taskapi.TaskProcess, tradeDetail *modtab.ExTradeDetail, tokenLock bool, single []GoDealStatus, index int) (bool, error) {
	if tokenLock { //锁仓上币，直接返回
		return false, nil
	}

	//通过计价货币折算交易量
	var (
		currencySymbol string  = tradeDetail.CoinSymbol
		currencyAmount float64 = tradeDetail.DealMoney
		mine_flag      bool    = false
		share_flag     bool    = true
	)

	if tp.GetCoinSymbolPair(tradeDetail.Pair) && time.Now().Unix() <= STOP_MINING_TIME_SECONDS {
		mine_flag = true
	}

	//手续费区分买入和卖出
	if tradeDetail.OrderSide == int(FREEZE_BID) {
		//买
		if tradeDetail.CoinSymbol == "CP" {
			share_flag = false
		}
	} else {
		//卖
		if tradeDetail.CurrencySymbol == "CP" {
			share_flag = false
		}
	}

	if tradeDetail.PayBix == int(PAY_BIX_FREEZE_YES_CP) {
		share_flag = false
	}

	price, err := tp.EqualBtcPrice(currencySymbol)
	if err != nil {
		logger.Errorf("tradeMonitor coinPriceInBTC err %s\n", err.Error())
		return false, err
	}

	if price.IsZero() {
		warnMsg := fmt.Sprintf("get ticker price undefined user_id:%d, currencySymbol:%s.", tradeDetail.UserID, currencySymbol)
		logger.Debugln(warnMsg)
		return false, errors.New(warnMsg)
	}

	equalBTC := decimal.NewFromFloat(currencyAmount).Mul(price).Truncate(8)
	user_id := tradeDetail.UserID

	//是否免手续费
	var isFree bool
	if tradeDetail.Fee <= 0 {
		isFree = true
	}

	tradeTime := tradeDetail.CreatedAt
	tradeDate := tradeTime.Unix() - tradeTime.Unix()%OneHour

	dataMap, ok := single[index].tradeDetailMap[user_id]
	if !ok {
		pHourTrade := NewTradeSet(nil, isFree, mine_flag, share_flag, equalBTC)
		pHourTrade.UserID = user_id
		pHourTrade.TradeDate = tradeDate

		dataMap = make(map[int64]*TradeSet)
		dataMap[tradeDate] = pHourTrade
		single[index].tradeDetailMap[user_id] = dataMap
	} else {
		pHourTrade, ok := dataMap[tradeDate]
		if !ok {
			pHourTrade := NewTradeSet(nil, isFree, mine_flag, share_flag, equalBTC)
			pHourTrade.UserID = user_id
			pHourTrade.TradeDate = tradeDate

			dataMap[tradeDate] = pHourTrade
		} else {
			NewTradeSet(pHourTrade, isFree, mine_flag, share_flag, equalBTC)
		}
	}

	return true, nil
}

func writeDailyTradeToDB(tp taskapi.TaskProcess, single []GoDealStatus, index int) (bool, *sql.Tx, error) {
	if (time.Now().Unix()-single[index].tradeDetailTime > TIME_OUT_UPDATE && len(single[index].tradeDetailMap) > 0) || len(single[index].tradeDetailMap) >= RECORD_SUM {
		lastPrice, err := tp.BtcEqucalSymbolPrice("BTC_USDT")
		if err != nil {
			logger.Errorf("BtcEqucalSymbolPrice err %s", err.Error())
			return false, nil, err
		}

		local_tradeDetail_map := single[index].tradeDetailMap
		single[index].tradeDetailMap = make(map[int64]map[int64]*TradeSet)

		tx, err := updteUserTradingAmount(tp, local_tradeDetail_map, lastPrice)
		if err != nil {
			return false, tx, err
		}

		single[index].tradeDetailTime = time.Now().Unix()

		return true, tx, nil //更新db
	}
	return false, nil, nil
}

func updteUserTradingAmount(tp taskapi.TaskProcess, tradeSetMap map[int64]map[int64]*TradeSet, price decimal.Decimal) (*sql.Tx, error) {
	var mErr error
	db, dbName, err := tp.GetSqlxIns()
	if err != nil {
		return nil, err
	}
	tx, err := db.Begin()
	if err != nil {
		logger.Errorf("updteUserTradingAmount Begin %s", err.Error())
		return nil, err
	}

	_, err = tx.Query(strings.Join([]string{"USE", dbName}, " "))
	if err != nil {
		logger.Errorf("updteUserTradingAmount use db:%s err:%s", dbName, err.Error())
		tx.Rollback()
		return tx, err
	}

	stmt, err := tx.Prepare(`insert into user_daily_trade
	(user_id, trade_date, equal_btc, equal_usdt, equal_btc_free,
	equal_usdt_free, equal_btc_mine,equal_usdt_mine, equal_btc_mine_free,
	equal_usdt_mine_free, equal_btc_share, equal_usdt_share,equal_btc_share_free,
	equal_usdt_share_free, createdAt, updatedAt) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
	ON DUPLICATE KEY UPDATE equal_btc=equal_btc+values(equal_btc),
	equal_usdt=equal_usdt+values(equal_usdt),equal_btc_free=equal_btc_free+values(equal_btc_free),
	equal_usdt_free=equal_usdt_free+values(equal_usdt_free),equal_btc_mine=equal_btc_mine+values(equal_btc_mine),
	equal_usdt_mine=equal_usdt_mine+values(equal_usdt_mine),equal_btc_mine_free=equal_btc_mine_free+values(equal_btc_mine_free),
	equal_usdt_mine_free=equal_usdt_mine_free+values(equal_usdt_mine_free),equal_btc_share=equal_btc_share+values(equal_btc_share),
	equal_usdt_share=equal_usdt_share+values(equal_usdt_share),equal_btc_share_free=equal_btc_share_free+values(equal_btc_share_free),
	equal_usdt_share_free=equal_usdt_share_free+values(equal_usdt_share_free)`)
	if err != nil {
		logger.Errorf("updteUserTradingAmount Prepare %s", err.Error())
		mErr = err
		goto end
	}

	//入库前的精度\Usdt计算
	for userID, dataMap := range tradeSetMap {
		for tradeDate, tradeSet := range dataMap {
			tradeTime := time.Unix(tradeDate, 0)
			equalBtc := tradeSet.EqualBtc.StringFixed(8)
			equalUsdt := tradeSet.EqualUsdt.Mul(price).StringFixed(8)
			equalBtcFree := tradeSet.EqualBtcFree.StringFixed(8)
			equalUsdtFree := tradeSet.EqualUsdtFree.Mul(price).StringFixed(8)
			equalBtcMine := tradeSet.EqualBtcMine.StringFixed(8)
			equalUsdtMine := tradeSet.EqualUsdtMine.Mul(price).StringFixed(8)
			equalBtcMineFree := tradeSet.EqualBtcMineFree.StringFixed(8)
			equalUsdtMineFree := tradeSet.EqualUsdtMineFree.Mul(price).StringFixed(8)
			equalBtcShare := tradeSet.EqualBtcShare.StringFixed(8)
			equalUsdtShare := tradeSet.EqualUsdtShare.Mul(price).StringFixed(8)
			equalBtcShareFree := tradeSet.EqualBtcShareFree.StringFixed(8)
			equalUsdtShareFree := tradeSet.EqualUsdtShareFree.Mul(price).StringFixed(8)

			_, err = stmt.Exec(
				userID,
				tradeTime,
				equalBtc,
				equalUsdt,
				equalBtcFree,
				equalUsdtFree,
				equalBtcMine,
				equalUsdtMine,
				equalBtcMineFree,
				equalUsdtMineFree,
				equalBtcShare,
				equalUsdtShare,
				equalBtcShareFree,
				equalUsdtShareFree,
				time.Now(), //这个日期bug 是否已有的数据 需要在更新吗
				time.Now())
			if err != nil {
				logger.Errorf("updteUserTradingAmount Exec %s", err.Error())
				mErr = err
				goto end
			}
		}
	}

end:
	if stmt != nil {
		stmt.Close() //runs here!
	}

	//now no todo commit
	// err = tx.Commit()
	// if err != nil {
	// 	logger.Errorf("updteUserTradingAmount Commit %s", err.Error())
	// 	return tx, err
	// }
	tradeSetMap = nil //标记gc销毁
	return tx, mErr
}

//更新日佣金
func updateDailyFee(tp taskapi.TaskProcess, tradeDetail *modtab.ExTradeDetail, tokenLock bool, single []GoDealStatus, index int) {
	var (
		feeCoinID     int
		feeCoinSymbol string
	)

	if tradeDetail.PayBix == int(PAY_BIX_FREEZE_YES_CP) {
		feeCoinID = int(gCPCoinID)
		feeCoinSymbol = "CP"
	} else {
		if tradeDetail.OrderSide == int(FREEZE_BID) {
			//买
			feeCoinID = tradeDetail.CoinID
			feeCoinSymbol = tradeDetail.CoinSymbol
		} else {
			//卖
			feeCoinID = tradeDetail.CurrencyID
			feeCoinSymbol = tradeDetail.CurrencySymbol
		}
	}

	tradeTime := tradeDetail.CreatedAt
	tradeDate := (tradeTime.Unix()) - (tradeTime.Unix())%OneHour

	if tokenLock {
		coinIdMap, ok := single[index].feeDetailLockMap[tradeDetail.UserID]
		if !ok {
			coinIdMap = make(map[int]map[int64]*TradeFeeSet)
			single[index].feeDetailLockMap[tradeDetail.UserID] = coinIdMap
		}

		tradeDateMap, ok := coinIdMap[feeCoinID]
		if !ok {
			tradeDateMap = make(map[int64]*TradeFeeSet)
			coinIdMap[feeCoinID] = tradeDateMap
		}

		tradeFee, ok := tradeDateMap[tradeDate]
		if !ok {
			tradeFee = &TradeFeeSet{}
			tradeFee.UserID = tradeDetail.UserID
			tradeFee.CoinID = feeCoinID    //币种id - user_daily_fee
			tradeFee.TradeDate = tradeDate //交易时间

			tradeFee.FeeCoinSymbol = feeCoinSymbol //手续费币种-token_lock_fee
			tradeFee.Fee = decimal.NewFromFloat(tradeDetail.Fee)

			/** only usefull on bibox */
			tradeFee.FeeEqualBTC = decimal.NewFromFloat(0)
			tradeFee.MakerFee = decimal.NewFromFloat(0)
			tradeFee.MakerFeeEqualBTC = decimal.NewFromFloat(0)
			/** */
			tradeFee.MakerEqualUsdtAmount = decimal.NewFromFloat(0)

			tradeFee.CoinSymbol = tradeDetail.CoinSymbol         //交易币种-token_lock_fee
			tradeFee.CurrencySymbol = tradeDetail.CurrencySymbol //计价币-token_lock_fee

			tradeFee.OrderSide = tradeDetail.OrderSide
			tradeFee.DealMoney = decimal.NewFromFloat(tradeDetail.DealMoney)
			tradeFee.DealAmount = decimal.NewFromFloat(tradeDetail.DealAmount) //成交数量-token_lock_fee

			tradeFee.EqualUsdtAmount = decimal.NewFromFloat(0) //挖矿+币币手续费（不包含免费上币）
			tradeFee.EqualUsdtReal = decimal.NewFromFloat(0)   //除去挖矿部分的手续费
			tradeDateMap[tradeDate] = tradeFee
		} else {
			tradeFee.Fee = tradeFee.Fee.Add(decimal.NewFromFloat(tradeDetail.Fee))
			tradeFee.DealMoney = tradeFee.DealMoney.Add(decimal.NewFromFloat(tradeDetail.DealMoney))
			tradeFee.DealAmount = tradeFee.DealAmount.Add(decimal.NewFromFloat(tradeDetail.DealAmount))
		}

		return
	} else {
		coinIdMap, ok := single[index].feeDetailAllMap[tradeDetail.UserID]
		if !ok {
			coinIdMap = make(map[int]map[int64]*TradeFeeSet)
			single[index].feeDetailAllMap[tradeDetail.UserID] = coinIdMap
		}

		tradeDateMap, ok := coinIdMap[feeCoinID]
		if !ok {
			tradeDateMap = make(map[int64]*TradeFeeSet)
			coinIdMap[feeCoinID] = tradeDateMap
		}

		tradeFee, ok := tradeDateMap[tradeDate]
		if !ok {
			tradeFee = &TradeFeeSet{}
			tradeFee.UserID = tradeDetail.UserID
			tradeFee.CoinID = feeCoinID
			tradeFee.TradeDate = tradeDate

			tradeFee.FeeCoinSymbol = feeCoinSymbol
			tradeFee.Fee = decimal.NewFromFloat(tradeDetail.Fee)

			/** only usefull on bibox */
			tradeFee.FeeEqualBTC = decimal.NewFromFloat(0)
			tradeFee.MakerFee = decimal.NewFromFloat(0)
			tradeFee.MakerFeeEqualBTC = decimal.NewFromFloat(0)
			/** */

			tradeFee.CoinSymbol = tradeDetail.CoinSymbol
			tradeFee.CurrencySymbol = tradeDetail.CurrencySymbol

			tradeFee.OrderSide = tradeDetail.OrderSide
			tradeFee.DealMoney = decimal.NewFromFloat(tradeDetail.DealMoney)
			tradeFee.DealAmount = decimal.NewFromFloat(tradeDetail.DealAmount)

			tradeFee.EqualUsdtAmount = decimal.NewFromFloat(0)
			tradeFee.EqualUsdtReal = decimal.NewFromFloat(0)
			tradeDateMap[tradeDate] = tradeFee
		} else {
			tradeFee.Fee = tradeFee.Fee.Add(decimal.NewFromFloat(tradeDetail.Fee))
			tradeFee.DealMoney = tradeFee.DealMoney.Add(decimal.NewFromFloat(tradeDetail.DealMoney))
			tradeFee.DealAmount = tradeFee.DealAmount.Add(decimal.NewFromFloat(tradeDetail.DealAmount))
		}
		return
	}
}

//每笔交易里有手续费统计
func writeFeeDetailLockToDB(tp taskapi.TaskProcess, tx *sql.Tx, single []GoDealStatus, index int, isUpdate bool) (bool, error) {
	if (time.Now().Unix()-single[index].feeDetailLockTime > TIME_OUT_UPDATE && len(single[index].feeDetailLockMap) > 0) || len(single[index].feeDetailLockMap) >= RECORD_SUM {
		lastPrice, err := tp.BtcEqucalSymbolPrice("BTC_USDT")
		if err != nil {
			return false, err
		}

		local_feeDetailLock_map := single[index].feeDetailLockMap
		single[index].feeDetailLockMap = make(map[int64]map[int]map[int64]*TradeFeeSet)

		err = updateTokenLockFee(tp, tx, local_feeDetailLock_map, lastPrice)
		if err != nil {
			return false, err
		}
		single[index].feeDetailLockTime = time.Now().Unix()
		return true, nil
	} else if isUpdate {
		if len(single[index].feeDetailLockMap) == 0 {
			return true, nil
		}

		lastPrice, err := tp.BtcEqucalSymbolPrice("BTC_USDT")
		if err != nil {
			return false, err
		}

		local_feeDetailLock_map := single[index].feeDetailLockMap
		single[index].feeDetailLockMap = make(map[int64]map[int]map[int64]*TradeFeeSet)

		err = updateTokenLockFee(tp, tx, local_feeDetailLock_map, lastPrice)
		if err != nil {
			return false, err
		}
		single[index].feeDetailLockTime = time.Now().Unix()
		return true, nil
	}
	return false, nil
}

// 更新用户日手续费 token_lock_fee  如果全部是锁币交易，则tx为nil，此函数独立更新数据
func updateTokenLockFee(tp taskapi.TaskProcess, tx *sql.Tx, tradeSetMap map[int64]map[int]map[int64]*TradeFeeSet, lastPrice decimal.Decimal) error {
	var mErr error
	var mTx *sql.Tx
	if tx == nil {
		db, dbName, err := tp.GetSqlxIns()
		if err != nil {
			return err
		}
		mTx, err = db.Begin()
		if err != nil {
			logger.Errorf("updateTokenLockFee token_lock_fee Begin %s", err.Error())
			return err
		}

		_, err = mTx.Query(strings.Join([]string{"USE", dbName}, " "))
		if err != nil {
			logger.Errorf("updateTokenLockFee use db:%s err:%s", dbName, err.Error())
			mTx.Rollback() //释放资源
			return err
		}
	} else {
		mTx = tx
	}

	stmt, err := mTx.Prepare(`insert into token_lock_fee(user_id,trade_date,fee_symbol,
	coin_symbol,currency_symbol,deal_amount,deal_amount_btc,deal_amount_usdt,equal_btc,
	equal_usdt,fee_amount,createdAt,updatedAt) values (?,?,?,?,?,?,?,?,?,?,?,?,?) 
	ON DUPLICATE KEY UPDATE deal_amount=values(deal_amount)+deal_amount, 
	deal_amount_btc=values(deal_amount_btc)+deal_amount_btc,deal_amount_usdt=values(deal_amount_usdt)+deal_amount_usdt,
	equal_btc=values(equal_btc)+equal_btc,equal_usdt=values(equal_usdt)+equal_usdt,
	fee_amount=values(fee_amount)+fee_amount,updatedAt=values(updatedAt)`)
	if err != nil {
		logger.Errorf("updateTokenLockFee Prepare %s", err.Error())
		mErr = err
		goto end
	}

	//入库前的精度
	for userID, coinIdMap := range tradeSetMap {
		for _, tradeDateMap := range coinIdMap {
			for tradeDate, tradeFee := range tradeDateMap {
				user_id := userID
				trade_date := time.Unix(tradeDate, 0)
				fee_symbol := tradeFee.FeeCoinSymbol       //手续费币种
				coin_symbol := tradeFee.CoinSymbol         //交易币种
				currency_symbol := tradeFee.CurrencySymbol //计价币
				deal_amount := tradeFee.DealAmount         //成交数量
				//price, err := coinPriceInBTC(tp, price2BTC, coin_symbol)
				price, err := tp.EqualBtcPrice(coin_symbol)
				if err != nil {
					mErr = err
					goto end
				}
				deal_amount_btc := tradeFee.DealAmount.Mul(price).StringFixed(10)
				deal_amount_usdt := tradeFee.DealAmount.Mul(price).Mul(lastPrice).StringFixed(10)
				//price, err = coinPriceInBTC(tp, price2BTC, fee_symbol)
				price, err = tp.EqualBtcPrice(fee_symbol)
				if err != nil {
					mErr = err
					goto end
				}
				equal_btc := price.Mul(tradeFee.Fee).StringFixed(10)
				equal_usdt := price.Mul(tradeFee.Fee).Mul(lastPrice).StringFixed(10)
				fee_amount := tradeFee.Fee
				_, err = stmt.Exec(user_id,
					trade_date,
					fee_symbol,
					coin_symbol,
					currency_symbol,
					deal_amount,
					deal_amount_btc,
					deal_amount_usdt,
					equal_btc,
					equal_usdt,
					fee_amount,
					time.Now(),
					time.Now())
				if err != nil {
					logger.Errorf("updateTokenLockFee Exec %s", err.Error())
					mErr = err
					goto end
				}
			}
		}
	}

end:
	if stmt != nil {
		stmt.Close() //runs here!
	}

	if tx == nil && mTx != nil {
		if mErr == nil {
			err = mTx.Commit()
			if err != nil {
				logger.Errorf("updateTokenLockFee Commit %s", err.Error())
				return err
			}
		} else {
			mTx.Rollback()
		}
	}

	tradeSetMap = nil //标记gc销毁
	return nil
}

//每笔交易里面有手续费
func writeFeeDetailAllToDB(tp taskapi.TaskProcess, tx *sql.Tx, single []GoDealStatus, index int, isUpdate bool) (bool, error) {
	if isUpdate {
		if len(single[index].feeDetailAllMap) == 0 {
			return true, nil
		}

		lastPrice, err := tp.BtcEqucalSymbolPrice("BTC_USDT")
		if err != nil {
			return false, err
		}

		local_feeDetailAll_map := single[index].feeDetailAllMap
		single[index].feeDetailAllMap = make(map[int64]map[int]map[int64]*TradeFeeSet)

		err = updateUserDailyFee(tp, tx, local_feeDetailAll_map, lastPrice)
		if err != nil {
			return false, err
		}
		single[index].feeDetailAllTime = time.Now().Unix()
		return true, nil
	}
	return false, nil
}

//user_daily_fee
func updateUserDailyFee(tp taskapi.TaskProcess, tx *sql.Tx, tradeSetMap map[int64]map[int]map[int64]*TradeFeeSet, lastPrice decimal.Decimal) error {
	var mErr error
	var mTx *sql.Tx
	if tx == nil {
		db, dbName, err := tp.GetSqlxIns()
		if err != nil {
			return err
		}

		mTx, err = db.Begin()
		if err != nil {
			logger.Errorf("updateUserDailyFee Begin %s", err.Error())
			return err
		}

		_, err = mTx.Query(strings.Join([]string{"USE", dbName}, " "))
		if err != nil {
			logger.Errorf("updateUserDailyFee use db:%s err:%s", dbName, err.Error())
			mTx.Rollback()
			return err
		}
	} else {
		mTx = tx
	}

	stmt, err := mTx.Prepare(`insert into user_daily_fee(user_id,coin_id,
		trade_date,coin_symbol,amount,equalbtc_amount,maker_amount,maker_equalbtc_amount,
		equalusdt_real,equalusdt_amount,maker_equalusdt_amount,createdAt,updatedAt)
		values (?,?,?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE amount=amount+values(amount),
		equalbtc_amount=equalbtc_amount+values(equalbtc_amount),maker_amount=maker_amount+values(maker_amount),
		maker_equalbtc_amount=maker_equalbtc_amount+values(maker_equalbtc_amount),equalusdt_real=equalusdt_real+values(equalusdt_real),
		equalusdt_amount=equalusdt_amount+values(equalusdt_amount),maker_equalusdt_amount=maker_equalusdt_amount+values(maker_equalusdt_amount)`)
	if err != nil {
		logger.Errorf("updateUserDailyFee Prepare %s", err.Error())
		mErr = err
		goto end
	}

	//入库前的精度
	for userID, coinIdMap := range tradeSetMap {
		for coinID, tradeDateMap := range coinIdMap {
			for tradeDate, tradeFee := range tradeDateMap {
				user_id := userID
				coin_id := coinID
				trade_date := time.Unix(tradeDate, 0)
				coin_symbol := tradeFee.CoinSymbol //交易币种 币种符号
				amount := tradeFee.Fee             //手续费

				//price, err := coinPriceInBTC(tp, price2BTC, tradeFee.FeeCoinSymbol)
				price, err := tp.EqualBtcPrice(tradeFee.FeeCoinSymbol) //手续费 交易币种
				if err != nil {
					mErr = err
					goto end
				}
				equalbtc_amount := tradeFee.Fee.Truncate(8).Mul(price).StringFixed(10)
				equalusdt_amount := tradeFee.Fee.Truncate(8).Mul(price).Mul(lastPrice).StringFixed(10)

				maker_amount := tradeFee.MakerFee                  //maker单的手续费
				maker_equalbtc_amount := tradeFee.MakerFeeEqualBTC //maker单的等于btc手续费

				var equalusdt_real string

				if tp.GetCoinSymbolPair(strings.Join([]string{tradeFee.CoinSymbol, tradeFee.CurrencySymbol}, "_")) == false {
					equalusdt_real = equalusdt_amount //除去挖矿部分的手续费
				}
				maker_equalusdt_amount := tradeFee.MakerEqualUsdtAmount //挖矿+币币手续费（不包含免费上币）

				_, err = stmt.Exec(user_id,
					coin_id,
					trade_date,
					coin_symbol,
					amount,
					equalbtc_amount,
					maker_amount,
					maker_equalbtc_amount,
					equalusdt_real,
					equalusdt_amount,
					maker_equalusdt_amount,
					time.Now(),
					time.Now())
				if err != nil {
					logger.Errorf("updateUserDailyFee Exec %s", err.Error())
					mErr = err
					goto end
				}
			}
		}
	}

end:
	if stmt != nil {
		stmt.Close() //runs here!
	}

	if tx == nil && mTx != nil {
		if mErr == nil {
			err = mTx.Commit()
			if err != nil {
				logger.Errorf("updateUserDailyFee Commit %s", err.Error())
				return err
			}
		} else {
			mTx.Rollback()
		}
	}

	tradeSetMap = nil //标记gc销毁
	return mErr
}
