统计用户交易量和手续费

kafka将ex_trade_detail表如下字段打包json发送到kafka中：
user_id：用户id
account_type：用户类型：普通账户0，信用账户1
pair：交易对
coin_id： 交易币种id
coin_symbol：交易币种符号
currency_id：计价币种id
currency_symbol：计价币种符号
order_side：交易方向 1：买（bid），2：卖（ask）
order_type：定价类型 1-市价， 2-限价
is_maker： 是否为maker
price：原始委托价格
amount：原始委托数量
money：原始委托金额
deal_price：成交价格
deal_amount： 成交数量
deal_money：成交金额
relay_id：关联委托单id
fee_rate：费率
fee：手续费
pay_bix：是否用bix计费

————————————————————————————————————————
附:
锁币币种：
查询select coin_symbol, shared_date from token_lock_proj WHERE status = ? and UNIX_TIMESTAMP(list_date) <= ?
status 等于2 在上线交易区，目前还没有分红的币种符号为锁仓币种

只要判断pair：交易对 
例如：CP_BTC 其中一个CP或BTC那么就是锁币

————————————————————————————————————————

order_side：交易方向 1：买（bid），2：卖（ask）

为买，交易币种为CP 则不分红
为卖，交易币种为CP 则不分红
	if tradeDetail.OrderSide == int(1) {
		//买
		if tradeDetail.CoinSymbol == "CP" {
			share_flag = false
		}
	} else {
		//卖
		if tradeDetail.CurrencySymbol == "CP" {
			share_flag = false
		}
	}

————————————————————————————————————————
pay_bix：是否用bix计费  //使用cp抵扣手续费 则不分红

if tradeDetail.PayBix == int(PAY_BIX_FREEZE_YES_CP) {
share_flag = false
}

————————————————————————————————————————

coin_symbol：交易币种符号  

如果为BTC则直接返回1

如果为CP：则查询redis

从redis获取 bibox_key_ticker.CP_BTC
CP 对应的BTC价格

{
  "pair": "CP_BTC",
  "time": 1556451186813,
  "price": 166,
  "amount": 589,
  "side": 2,
  "id": 2113250
}

bibox_key_ticker.CP_BTC 类型解释：
使用后面的BTC 166个对应一个CP这样的格式解释

————————————————————————————————————————
deal_amount： 成交数量

price 等于CP对应的BTC价格
equalBTC := decimal.NewFromFloat(currencyAmount).Mul(price).Truncate(8)
————————————————————————————————————————

currency_id：计价币种id
currency_symbol：计价币种符号

————————————————————————————————————————