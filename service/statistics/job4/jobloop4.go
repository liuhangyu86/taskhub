package job3

import (
	loger "taskhub/common/loger"
	taskapi "taskhub/service"
)

const (
	logModule = "job4"
)

var logger = loger.NewLogger(logModule)

type JobLoop4 struct {
	Index   int
	TaskPro taskapi.TaskProcess
	Stats   taskapi.JobStatus
}

func (this JobLoop4) Run() {
	item := this.TaskPro.LoopsConfig()[this.Index]
	logger.Debugf("%s", "job4 begin runing......")

	logger.Debugf("job4 loops:%d Name:%s Desc:%s Cron:%s",
		this.Index,
		item.Name,
		item.Desc,
		item.Cron,
	)

	logger.Debugf("%s", "job4 end")
}
